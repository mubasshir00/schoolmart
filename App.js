import React from "react";
import { StyleSheet } from "react-native";
import "react-native-gesture-handler";
import { NativeBaseProvider, View } from "native-base";
import Main from "./src/components/Navigators/Main";
import { NavigationContainer } from "@react-navigation/native";
import TestScreen from "./TestScreen";
import { Provider } from "react-redux";
import store from "./src/Redux/store";
import { LogBox } from "react-native";

// import { ProductsProvider } from './src/context/products_context';
// import { FilterProvider } from './src/context/filter_context';
// import { CartProvider } from './src/context/cart_context';

//context api
import Auth from "./src/context/store/Auth";
LogBox.ignoreLogs([
  "Non-serializable values were found in the navigation state",
]);
const App = () => {
  return (
    <Auth>
      <Provider store={store}>
        <NativeBaseProvider>
          <NavigationContainer>
            <Main styles={styles.commonstyle} />
          </NavigationContainer>
        </NativeBaseProvider>
      </Provider>
    </Auth>
  );
};

export default App;

const styles = StyleSheet.create({
  navContainer: {},
});

// color: #f01c2c

//  <NavigationContainer style={styles.navContainer}>
//         <NavBar/>
//
//         <MyTab/>
//       </NavigationContainer>
