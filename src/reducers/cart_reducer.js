import { ADD_TO_CART ,CLEAR_CART,COUNT_CART_TOTALS,REMOVE_CART_ITEM,TOGGLE_CART_ITEM_AMOUNT } from "../actions";

const cart_reducer = (state,action) =>{
    if(action.type === ADD_TO_CART){
        const { id, price, name, image} = action.payload;
        // console.log('aaaa',action.payload);
        const tempItem = state.cart.find((i) => i.id === id)
        // console.log(state.cart);

        if(tempItem){
          return {...state}
        } else {
            const newItem = {
                id: id ,
                name: name,
                price :price,
                image: image
            }
            // console.log('aaa',newItem);
            return {...state,cart:[...state.cart,newItem]}
        }
        // console.log(state.cart);

    }

}

export default cart_reducer