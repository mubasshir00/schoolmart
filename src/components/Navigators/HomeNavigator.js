import { createStackNavigator } from "@react-navigation/stack";
import React from "react";
import { StyleSheet, TouchableOpacity } from "react-native";
import { Box } from "native-base";
import Ionicons from "react-native-vector-icons/Ionicons";

import HomeScreen from "../../screens/HomeScreen";
import {
  BackpackScreen,
  CompetitionScreen,
  CourseTrainingScreen,
  FeesPayScreen,
  FoodScreen,
  GamesScreen,
  GiftsScreen,
  HealthCareScreen,
  HospitalityScreen,
  InsuranceScreen,
  JobsScreen,
  LibraryScreen,
  LoanScreen,
  MobileRechargeScreen,
  MoreScreen,
  SavingsScreen,
  ShoppingScreen,
  StationaryScreen,
  TechnologyScreen,
  TravelScreen,
  WelfareScreen,
} from "../../screens/StackScreens";

import SingleBook from "../LibraryComponents/Books/SingleBook";
import { AppProvider } from "../Quiz/context";
import AllBook from "../../components/LibraryComponents/Books/AllBook";

import Quiz from "../QuizComponent/LiveQuiz";
import { Text } from "native-base";
import SearchIcon from "../SearchScreen/SearchIcon";
import MyCart from "../../screens/StackScreens/MyCart";
// import Checkout from '../../screens/StackScreens/Checkout';
import ScienceTech from "../LibraryComponents/BookCategory/ScienceTech";
import AcademicBook from "../LibraryComponents/BookCategory/AcademicBook";
import StoryNovel from "../LibraryComponents/BookCategory/StoryNovel";
import DramaPoetry from "../LibraryComponents/BookCategory/DramaPoetry";
import BackpackCategory from "../BackPack/BackpackCategory";
import CourseTrainingCategory from "../Course&Training/CourseTrainingCategory";
import SingleCourse from "../Course&Training/SingleCourse";
import SingleBackPack from "../BackPack/SingleBackPack";
import QuizPage from "../Quiz/QuizPage";
import SingleGift from "../Gifts/SingleGift";
import LoginScreen from "../../Authentication/LoginScreen";
import SignupScreen from "../../Authentication/SignupScreen";
import SearchScreen from "../SearchScreen";
import WhiteScreen from "../SearchScreen/WhiteScreen";
import { useNavigation } from "@react-navigation/native";
import UserProfile from "../../Authentication/UserProfile";
import PersonalInformation from "../PersonalInformation/PersonalInformation";
import Order from "../Order/Order";
import Checkout from "../../Cart/Checkout";
import Payment from "../../Cart/Payment";
import Confirm from "../../Cart/Confirm";

const quizCom = () => {
  return (
    <AppProvider>
      <Quiz />
    </AppProvider>
  );
};

const Stack = createStackNavigator();

function MyStack() {
  const navigation = useNavigation();
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Home"
        component={HomeScreen}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="Backpack"
        component={BackpackScreen}
        options={
          ({
            headerShown: false,
          },
          ({ route }) => ({ title: route.params.name }),
          ({ navigation }) => ({
            headerRight: () => <SearchIcon />,
          }))
        }
      />
      {/* <Stack.Screen name="Login" component={LoginScreen} options={{
        headerShown:true
      }}/> */}
      <Stack.Screen
        name="Competition"
        component={CompetitionScreen}
        options={{
          headerShown: true,
        }}
      />
      <Stack.Screen
        name="Course & Training"
        component={CourseTrainingScreen}
        options={{
          headerShown: true,
        }}
      />
      <Stack.Screen
        name="Fees Pay"
        component={FeesPayScreen}
        options={{
          headerShown: true,
        }}
      />
      <Stack.Screen
        name="Food"
        component={FoodScreen}
        options={
          ({
            headerShown: false,
          },
          ({ route }) => ({ title: route.params.name }),
          ({ navigation }) => ({
            headerRight: () => <SearchIcon />,
          }))
        }
      />
      <Stack.Screen
        name="Sports & Games"
        component={GamesScreen}
        options={{
          headerShown: true,
        }}
      />
      <Stack.Screen
        name="Gifts"
        component={GiftsScreen}
        options={
          ({
            headerShown: false,
          },
          ({ route }) => ({ title: route.params.name }),
          ({ navigation }) => ({
            headerRight: () => <SearchIcon />,
          }))
        }
      />
      <Stack.Screen
        name="Healthcare"
        component={HealthCareScreen}
        options={{
          headerShown: true,
        }}
      />
      <Stack.Screen
        name="Hospitality"
        component={HospitalityScreen}
        options={{
          headerShown: true,
        }}
      />
      <Stack.Screen
        name="Insurance"
        component={InsuranceScreen}
        options={{
          headerShown: true,
        }}
      />
      <Stack.Screen
        name="Jobs"
        component={JobsScreen}
        options={{
          headerShown: true,
        }}
      />
      <Stack.Screen
        name="Library"
        component={LibraryScreen}
        options={
          ({
            headerShown: false,
          },
          ({ route }) => ({ title: route.params.name }),
          ({ navigation }) => ({
            headerRight: () => <SearchIcon />,
          }))
        }
      />
      <Stack.Screen
        name="Loan"
        component={LoanScreen}
        options={{
          headerShown: true,
        }}
      />
      <Stack.Screen
        name="Mobile Recharge"
        component={MobileRechargeScreen}
        options={{
          headerShown: true,
        }}
      />
      <Stack.Screen
        name="More"
        component={MoreScreen}
        options={{
          headerShown: true,
        }}
      />
      <Stack.Screen
        name="Savings"
        component={SavingsScreen}
        options={{
          headerShown: true,
        }}
      />
      <Stack.Screen
        name="Shopping"
        component={ShoppingScreen}
        options={
          ({
            headerShown: false,
          },
          ({ route }) => ({ title: route.params.name }),
          ({ navigation }) => ({
            headerRight: () => <SearchIcon />,
          }))
        }
      />
      <Stack.Screen
        name="Stationary"
        component={StationaryScreen}
        options={
          ({
            headerShown: false,
          },
          ({ route }) => ({ title: route.params.name }),
          ({ navigation }) => ({
            headerRight: () => <SearchIcon />,
          }))
        }
      />
      <Stack.Screen
        name="Technology"
        component={TechnologyScreen}
        options={{
          headerShown: true,
        }}
      />
      <Stack.Screen
        name="Travel"
        component={TravelScreen}
        options={{
          headerShown: true,
        }}
      />
      <Stack.Screen
        name="Welfare"
        component={WelfareScreen}
        options={{
          headerShown: true,
        }}
      />
      <Stack.Screen
        name="Live Quiz"
        component={quizCom}
        options={{
          headerShown: true,
        }}
      />
      <Stack.Screen
        name="Single Book"
        component={SingleBook}
        options={
          ({
            headerShown: true,
            headerTitleAlign: "center",
          },
          ({ route }) => ({ title: route.params.name }),
          ({ navigation }) => ({
            // headerRight: () => <LibraryCart />,
          }))
        }
      />

      <Stack.Screen name="User Profile" component={UserProfile} />

      <Stack.Screen name="All Book" component={AllBook} />
      <Stack.Screen name="My Cart" component={MyCart} />
      <Stack.Screen name="Checkout" component={Checkout} />

      <Stack.Screen name="Science & Tech" component={ScienceTech} />
      <Stack.Screen name="Academic Book" component={AcademicBook} />
      <Stack.Screen name="Story & Novel" component={StoryNovel} />
      <Stack.Screen name="Drama & Poetry" component={DramaPoetry} />

      <Stack.Screen name="Payment" component={Payment} />
      <Stack.Screen name="Confirm" component={Confirm} />

      <Stack.Screen name="Backpack Category" component={BackpackCategory} />
      <Stack.Screen
        name="SingleBackPack"
        component={SingleBackPack}
        options={
          ({
            headerShown: true,
            headerTitleAlign: "center",
          },
          ({ route }) => ({ title: route.params.name }),
          ({ navigation }) => ({
            headerRight: () => <SearchIcon />,
          }))
        }
      />

      <Stack.Screen
        name="CourseTrainingCategory"
        component={CourseTrainingCategory}
      />
      <Stack.Screen
        name="SingleCourse"
        component={SingleCourse}
        options={
          ({
            headerShown: true,
            headerTitleAlign: "center",
          },
          ({ route }) => ({ title: route.params.name }),
          ({ navigation }) => ({
            headerRight: () => <SearchIcon />,
          }))
        }
      />
      <Stack.Screen
        name="SingleGifts"
        component={SingleGift}
        options={
          ({
            headerShown: true,
            headerTitleAlign: "center",
          },
          ({ route }) => ({ title: route.params.name }))
        }
      />
      <Stack.Screen name="Quiz" component={QuizPage} />
      <Stack.Screen name="Sign Up" component={SignupScreen} />
      <Stack.Screen name="Sign In" component={LoginScreen} />
      <Stack.Screen
        name="Search Student Mart"
        component={WhiteScreen}
        options={{
          title: <SearchScreen />,
          // backgroundColor:'#f01c2c'
        }}
      />
      <Stack.Screen
        name="Personal Information"
        component={PersonalInformation}
      />
      <Stack.Screen name="My Order" component={Order} />
    </Stack.Navigator>
  );
}

export default function HomeNavigator() {
  return <MyStack />;
}

const styles = StyleSheet.create({});
