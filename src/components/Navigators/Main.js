import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {Box, Text} from 'native-base';
import React from 'react';
import {StyleSheet, View} from 'react-native';
import InboxScreen from '../../screens/InboxScreen';
import QrCodeScreen from '../../screens/QrCodeScreen';
import HomeNavigator from './HomeNavigator';
import NotificationScreen from './../../screens/StackScreens/NotificationScreen'

import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import CartNavigator from './CartNavigator';
import Ionicons from 'react-native-vector-icons/Ionicons'
import ProfileScreen from './../../screens/StackScreens/ProfileScreen'
import CartIcon from './../../components/ReUsebleComponent/CartIcon'
const Tab = createBottomTabNavigator();

const Main = () => {
  return (
    <Tab.Navigator
      initialRouteName="Home"
      screenOptions={{
        keyboardHidesTabBar: true,
        tabBarShowLabel: false,
        tabBarActiveTintColor: 'red',
      }}>
      <Tab.Screen
        name="home"
        component={HomeNavigator}
        options={{
          tabBarIcon: ({color}) => (
            <FontAwesome
              name="home"
              style={{position: 'relative'}}
              color={color}
              size={30}
            />
          ),
          tabBarShowLabel: false,
          headerTitleAlign: 'center',
          headerShown: false,
        }}
      />
      <Tab.Screen 
      name="Cart"
      component={CartNavigator}
      options={{
        tabBarIcon:({color}) =>(
          <View>
            <FontAwesome name="shopping-cart" color={color} size={30}/>
            <CartIcon/>
          </View>
        ),
        headerShown:false
      }}
      />
      <Tab.Screen 
      name="Notification"
        component={NotificationScreen}
        options={{
          tabBarIcon:({color}) =>(
          <View>
              <Ionicons name="notifications" color={color} size={30}/>
          </View>
          )
        }}
      />
      <Tab.Screen
        name="Profile"
        component={ProfileScreen}
        options={{
          tabBarIcon: ({ color }) => (
            <View>
              <FontAwesome name="user" color={color} size={30} />
            </View>
          )
        }}
      />
      
    </Tab.Navigator>
  );
};

export default Main;

const styles = StyleSheet.create({});
