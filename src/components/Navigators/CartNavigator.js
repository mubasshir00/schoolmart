import { StyleSheet, Text, View } from 'react-native';
import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import Cart from '../../Cart/Cart';
import CheckoutNavigator from './CheckoutNavigator';
const Stack = createStackNavigator()
const MyStack = () => {
  return (
    <Stack.Navigator>
    <Stack.Screen name="cart" component={Cart} 
    options={{
        headerShown:false
    }}
    />
    {/* <Stack.Screen 
    name="Checkout"
    component={CheckoutNavigator}
    options={{
        title:'Checkout'
    }}
    /> */}
    </Stack.Navigator>
  );
};

export default function CartNavigator(){
    return <MyStack/>
};

const styles = StyleSheet.create({});
