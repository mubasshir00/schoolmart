import React from 'react';
import {createDrawerNavigator} from '@react-navigation/drawer'
import { NavigationContainer } from '@react-navigation/native'
import ProfileScreen from '../../screens/StackScreens/ProfileScreen';
import { StyleSheet, Text, View } from 'react-native'
import HomeScreen from '../../screens/HomeScreen';
import Main from './Main';

const Drawer = createDrawerNavigator();

function MyDrawer(){
    console.log('success');
    return(
    <Drawer.Navigator initialRouteName='main'>
        <Drawer.Screen name="main" component={Main}/>
        <Drawer.Screen name="Profile Screen" component={ProfileScreen}/>
    </Drawer.Navigator>
    )
}

export default function DrawerNavigator(){
    return <MyDrawer/>
}

const styles = StyleSheet.create({});
