
import React, { Component } from 'react'
import { SafeAreaView, ScrollView, Text, TouchableOpacity, View } from 'react-native'
import { RNCamera } from 'react-native-camera'
import QRCodeScanner from 'react-native-qrcode-scanner'
import QRCode from 'react-native-qrcode-svg';

export class QrCodeScanner extends Component {
    state = {
        qr:""
    }
    onRead = e =>{
        this.setState({qr:e.data})
        // console.log(e);
    }
    render() {
        return (
    <SafeAreaView>
        <ScrollView contentInsetAdjustmentBehavior="automatic">
        <View>
            <QRCodeScanner
               onRead={this.onRead}
               flashMode={RNCamera.Constants.FlashMode.torch}
              ref={(node) => { this.scanner = node }}
        bottomContent={
          <TouchableOpacity>
            <Text >OK. Got it!</Text>
          </TouchableOpacity>
        }
      />
       {
           this.state.qr ? <QRCode
           value={this.state.qr}
           />:null
       }
            </View>
            <View>
                <Text>{this.state.qr}</Text>
            </View>
               </ScrollView>
           </SafeAreaView>
        )
    }
}

export default QrCodeScanner