import { Dimensions, StyleSheet, Text, TextInput, View } from "react-native";
import React from "react";
const { width, height } = Dimensions.get("window");
const Input = props => {
  return (
    <TextInput
      style={styles.input}
      placeholder={props.placeholder}
      name={props.name}
      id={props.id}
      value={props.value}
      autoCorrect={props.autoCorrect}
      onChangeText={props.onChangeText}
      onFocus={props.onFocus}
      secureTextEntry={props.secureTextEntry}
      keyboardType={props.keyboardType}
    ></TextInput>
  );
};

export default Input;

const styles = StyleSheet.create({
  input: {
    width: width / 1.2,
    height: 40,
    backgroundColor: "white",
    marginVertical: 4,
    padding: 10,
    borderRadius: 10,
    borderWidth: 2,
    borderColor: "#f01c2c",
  },
});
