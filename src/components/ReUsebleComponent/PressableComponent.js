import {useNavigation} from '@react-navigation/native';
import {Box, Pressable, Text} from 'native-base';
import React from 'react';
import {StyleSheet, View} from 'react-native';

const PressableComponent = ({children, ...props}) => {
  const navigation = useNavigation();
  return (
    <Pressable >
      {({isHovered, isFocused, isPressed}) => {
        return (
          <Box
            bg={
              isPressed
                ? `${props.pressedColor}`
                : isHovered
                ? `${props.hoveredColor}`
                : `${props.hoveredColor}`
            }
            p={props.p}
            rounded={props.rounded}
            style={{
              transform: [
                {
                  scale: isPressed ? 0.96 : 1,
                },
              ],
            }}>
            {children}
          </Box>
        );
      }}
    </Pressable>
  );
};

export default PressableComponent;

const styles = StyleSheet.create({});
