import { Box } from 'native-base';
import React from 'react';
import { useNavigation } from '@react-navigation/native';

import { Dimensions, TouchableOpacity } from 'react-native';
const {width,height} = Dimensions.get('window')
function Card(props) {
    const navigation = useNavigation();

    console.log(props);

  return(
      <TouchableOpacity
      width={{
          width:width/2.5,
              justifyContent: 'center',
              alignItems: 'center',
      }}
    //   onPress={()=> navigation.navigate({props.navigationName},{item:props.item})}
      >
    <Box my="2" paddingBottom="1" style={{ width: width / 2.8, backgroundColor: 'white' }}>
        {props.children}
    </Box>
    </TouchableOpacity>
  );
}

export default Card;
