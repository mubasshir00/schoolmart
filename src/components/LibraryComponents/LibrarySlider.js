import {Box, Image, ScrollView, Text} from 'native-base';
import React, {useEffect, useState} from 'react';
import {Dimensions, StyleSheet, View} from 'react-native';
import Swiper from 'react-native-swiper';
var {width,height} = Dimensions.get('window');

const LibrarySlider = () => {
  const [banner, setBanner] = useState([]);

  useEffect(() => {
    setBanner([
      'https://images.pexels.com/photos/733854/pexels-photo-733854.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940',
      'https://images.pexels.com/photos/1340588/pexels-photo-1340588.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940',
      'https://images.pexels.com/photos/1516983/pexels-photo-1516983.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=940',
    ]);
    return () => {
      setBanner([]);
    };
  }, []);

  return (
    <ScrollView background="info.50">
      <View style={styles.container}>
        <View style={styles.swiper}>
          <Swiper
            style={{height: width / 2.4}}
            showButtons={false}
            autoplay={true}
            autoplayTimeout={2}>
            {banner.map(item => {
              return (
                <Image
                  key={item}
                  style={styles.imageBanner}
                  resizeMode="cover"
                  source={{uri: item}}
                  alt="text"
                />
              );
            })}
          </Swiper>
          <View style={{height: 1}} />
        </View>
      </View>
    </ScrollView>
  );
};

export default LibrarySlider;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // backgroundColor: 'gainsboro',
  },
  swiper: {
    width: width,
    alignItems: 'center',
    // marginTop: 10,
  },
  imageBanner: {
    height: width / 2,
    width: width,
    // borderRadius: 10,
    // marginHorizontal: 20,
  },
});
