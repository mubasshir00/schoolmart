import { Box, ScrollView, Text } from 'native-base';
import React from 'react'
import { Dimensions, StyleSheet, View } from 'react-native'
import BookCard from '../Books/BookCard';
const {width} = Dimensions.get('window')
const AcademicBook = ({ route, navigation }) => {
    const { data } = route.params;
    console.log('data', data);
    // console.log('a',route.params);
    return (
        <ScrollView>
            <Box
                justifyContent="center"
                flex="1"
                flexDirection="row"
                alignItems="flex-start"
                flexWrap="wrap"
                my="2"
            >
                {
                    data.map((item) => {
                        // const {name} =item;
                        return (
                            <Box 
                                style={{
                                    width: width / 2.1,
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    paddingHorizontal:3,
                                    paddingVertical:5
                                }}
                                >
                                <BookCard {...item} />
                            </Box>
                        )
                    })
                }
            </Box>
        </ScrollView>
    )
}

export default AcademicBook

const styles = StyleSheet.create({})
