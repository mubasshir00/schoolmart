import { useNavigation } from '@react-navigation/native';
import { Box, FlatList, Heading, Icon, Input, ScrollView, Text } from 'native-base';
import React, { useEffect, useState } from 'react';
import { StyleSheet, View, TouchableOpacity, Dimensions } from 'react-native';
import BookList from './Books/BookList';

const data = require('../../assets/LibraryAssets/Book.json');

const bookCategories = require('../../assets/LibraryAssets/categories.json');

import Ionicons from 'react-native-vector-icons/Ionicons'
import CategoryFilter from './Books/CategoryFilter';
import SearchedBooks from './Books/SearchedBooks';
import LibrarySlider from './LibrarySlider';
import BookContainer from './Books/BookContainer';

const { width } = Dimensions.get('window')

const LibraryCom = ({ libraryProduct}) => {
    console.log('libraryProduct', libraryProduct);
    const [books, setBooks] = useState([]);
    const [categories, setCategories] = useState([]);
    const [active, setActive] = useState();
    const [booksCtg, setBooksCtg] = useState('all');
    const [initialState, setInitialState] = useState([]);
    const [booksFiltered, setBooksFiltered] = useState([]);
    const [focus, setFocus] = useState();

    const navigation = useNavigation();

    useEffect(() => {
        setBooks(libraryProduct);
        setCategories(bookCategories);
        setActive(-1);
        setInitialState(data);
        setBooksFiltered(data);
        setFocus(false);

        return () => {
            setBooks([]);
            setCategories([]);
            setActive();
            setInitialState([]);
            setFocus();
            setBooksFiltered([]);
        };
    }, [libraryProduct]);

    // console.log('books',books);

    //search books 
    const searchBook = (text) => {
        setBooksFiltered(
            books.filter((i) => i.name.toLowerCase().includes(text.toLowerCase()))
        );
    };


    const openList = () => {
        setFocus(true);
    }

    const onBlur = () => {
        setFocus(false)
    }

    //categories
    const changeCtg = (ctg) => {
        {
            ctg === 'all'
                ? [setBooksCtg(initialState), setActive(true)]
                : [
                    setBooksCtg(
                        books.filter((i) => i.category._id === ctg),
                    ),
                ];
        }
    };

    return (
        <Box background="info.50">

        <Box flexDirection="row" justifyContent="center" alignItems="center" marginBottom="0">
                
                {focus == true ? <Ionicons onPress={onBlur} size={30} name="close-outline" /> : null}
            </Box>

            {
            focus == true ? (
            <SearchedBooks
            booksFiltered={booksFiltered}
            />
        ) : (
        <View>

        <LibrarySlider />

        <BookContainer />

        <View>
        <CategoryFilter/>
        </View>

        {
        booksCtg?.length > 0 ? (
        <ScrollView background="info.50">
        <Box
            justifyContent="center"
            flex="1"
            flexDirection="row"
            alignItems="flex-start"
            flexWrap="wrap"
            my="3"
        >
        {books.map(item => {
        return <BookList key={item.id + Math.random().toString()} item={item} />;
        })}
        </Box>
        </ScrollView>
        ) : (
        <Box>
            <Text>No Book found</Text>
        </Box>
            )
        }

        </View>
        )
        }
        </Box>
    )
}

export default LibraryCom

const styles = StyleSheet.create({})
