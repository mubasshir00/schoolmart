import { useNavigation } from '@react-navigation/native'
import { Box, Image, List, ScrollView, Text } from 'native-base'
import React from 'react'
import { Dimensions, StyleSheet, TouchableOpacity, View } from 'react-native'
const {width} = Dimensions.get('window')
const SearchedBooks = (props) => {
    const { booksFiltered} =props;
    const navigation = useNavigation();
    console.log(props);
    return (
        <ScrollView style={{width:width}}>
    {
    booksFiltered.length > 0 ? (
    booksFiltered.map((item)=>(
        <List
        px="4" key={item.id*Math.random()+Date.now().toString()}>
        <TouchableOpacity
            onPress={()=>{
            navigation.navigate('Single Book',{item:item})
            }}
    >
    <Box flexDirection="row" alignItems="center">
    <Box marginRight="2">
    <Image width="10" height="30" source={{ uri: item.image }} alt="image" />
    </Box>
    <Box>
    <Text>{item.name}</Text>
    <Text color="grey" fontSize="10">{item.writer}</Text>
    </Box>
    </Box>
    </TouchableOpacity>
    </List>
        ))
    ) : (
                    <View>
                        <Text style={{alignSelf:'center'}}>
                            No Books Match
                        </Text>
                    </View>
                )
            }
        </ScrollView>
    )
}

export default SearchedBooks

const styles = StyleSheet.create({})
