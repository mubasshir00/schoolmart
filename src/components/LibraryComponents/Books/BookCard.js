import { Box, Image, Text } from "native-base";
import React, { useContext, useState } from "react";
import { Dimensions, StyleSheet, View, TouchableOpacity } from "react-native";
import FontAweSome5 from "react-native-vector-icons/FontAwesome5";
import { connect } from "react-redux";
import AuthGlobal from "../../../context/store/AuthGlobal";
// import { useCartContext } from '../../../context/cart_context';
import * as actions from "../../../Redux/Actions/cartActions";
import Toast from "react-native-simple-toast";
var { width } = Dimensions.get("window");

const BookCard = props => {
  const context = useContext(AuthGlobal);
  const {
    id,
    image,
    brand,
    price,
    rating,
    numReviews,
    isFeatured,
    auther,
    name,
  } = props;
  // console.log(_id);
  return (
    <Box
      padding="1"
      key={id + Math.random().toString() + Date.now().toString()}
    >
      <Image
        source={{
          uri: "https://i.pinimg.com/736x/db/3c/a7/db3ca70015d218194757e1cfb00d114a.jpg",
        }}
        resizeMode="cover"
        style={styles.image}
        alt="name"
        borderRadius="5"
      />
      <Box />
      <Text fontSize="15" fontWeight="bold">
        {name?.length > 16 ? name.substring(0, 16 - 3) + "..." : name}
      </Text>
      <Text color="grey" fontSize="13">
        {auther
          ? auther?.length > 15
            ? auther.substring(0, 15 - 3) + "..."
            : auther
          : "No Name Provided"}
      </Text>
      <Text marginBottom="1" fontSize="12" fontWeight="bold">
        {price} BDT
      </Text>
      {context.stateUser.user.userId ? (
        <TouchableOpacity
          onPress={() => {
            props.addItemToCart(props);
          }}
        >
          <Box
            backgroundColor="#f01c2c"
            borderRadius="2"
            // borderWidth="2"
            flexDirection="row"
            alignItems="center"
            justifyContent="center"
          >
            <FontAweSome5 name="cart-plus" color="white" />
            <Text marginLeft="1" fontWeight="bold" color="white">
              Add To Cart
            </Text>
          </Box>
        </TouchableOpacity>
      ) : (
        <TouchableOpacity
          onPress={() => {
            Toast.show("Your Must Login", Toast.SHORT, ["UIAlertController"]);
          }}
        >
          <Box
            backgroundColor="#f01c2c"
            borderRadius="2"
            // borderWidth="2"
            flexDirection="row"
            alignItems="center"
            justifyContent="center"
          >
            <FontAweSome5 name="cart-plus" color="white" />
            <Text marginLeft="1" fontWeight="bold" color="white">
              Add To Cart
            </Text>
          </Box>
        </TouchableOpacity>
      )}
    </Box>
  );
};

const mapDispatchToProps = dispatch => {
  return {
    addItemToCart: product =>
      dispatch(actions.addToCart({ quantity: 1, product })),
  };
};

export default connect(null, mapDispatchToProps)(BookCard);

const styles = StyleSheet.create({
  container: {
    width: width,
    height: width / 1.7,
    padding: 10,
    borderRadius: 10,
    marginTop: 55,
    marginBottom: 5,
    marginLeft: 10,
    alignItems: "center",
    elevation: 8,
  },
  image: {
    width: width / 1.1,
    height: width / 2.6,
  },
  card: {
    marginBottom: 10,
    height: width / 2 - 20 - 9,
    backgroundColor: "white",
    width: width / 2 - 20 - 10,
  },
  btnContainer: {
    marginRight: 10,
  },
});
