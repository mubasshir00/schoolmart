import { Box, Image, ScrollView, Text } from "native-base";
import React, { useContext, useEffect, useState } from "react";
import { Dimensions, StyleSheet, View, TouchableOpacity } from "react-native";
import AntDesign from "react-native-vector-icons/AntDesign";
import FontAwesomeIcon from "react-native-vector-icons/FontAwesome5";
import { connect } from "react-redux";
import AuthGlobal from "../../../context/store/AuthGlobal";
import Toast from "react-native-simple-toast";

// import { useCartContext } from '../../../context/cart_context';
const { width, height } = Dimensions.get("screen");
// console.log(width);

import * as actions from "../../../Redux/Actions/cartActions";

const SingleBook = props => {
  // const { addToCart} = useCartContext()

  console.log(props);

  const context = useContext(AuthGlobal);

  const [book, setBook] = useState(props.route.params.item);
  console.log(book);
  const {
    id,
    image,
    publisher,
    totalrating,
    price,
    numReviews,
    auther,
    richDescription,
    name,
    des,
  } = book;
  const ratingAv = 4;

  useEffect(() => {
    props.navigation.setOptions({
      title: name,
    });
  }, [name]);

  const totalPage = "300";
  const release = "20";
  const language = "English";

  console.log(ratingAv);
  return (
    <Box background="info.50">
      <ScrollView
        contentContainerStyle={{
          alignItems: "center",
          height: "90%",
        }}
      >
        <Box style={styles.imageContainer}>
          <Image
            source={{
              uri: "https://i.pinimg.com/736x/db/3c/a7/db3ca70015d218194757e1cfb00d114a.jpg",
            }}
            rezieMode="cover"
            alt="a"
            width={width / 2.1}
            height={width / 1.4}
          />
        </Box>
        <Text fontSize="17" fontWeight="bold" textAlign="center">
          {name}
        </Text>
        <Text color="grey" fontSize="15" marginTop="0.5">
          {auther
            ? auther?.length > 15
              ? auther.substring(0, 15 - 3) + "..."
              : auther
            : "No Name Provided"}
        </Text>

        <Box flexDirection="row">
          <Text>
            {Array(ratingAv)
              .fill()
              .map((_, i) => (
                <Box px="0.5" key={i}>
                  <AntDesign name="star" color="orange" size={13} />
                </Box>
              ))}
          </Text>
          <Text>
            {Array(5 - ratingAv)
              .fill()
              .map((_, i) => (
                <Box px="0.5" key={i}>
                  <AntDesign name="staro" size={13} />
                </Box>
              ))}
          </Text>
        </Box>
        <Box marginTop="2" flexDirection="row">
          <Box width={width / 4} alignItems="center">
            <Text fontSize="19" fontWeight="bold">
              {totalPage}
            </Text>
            <Text color="grey">Page</Text>
          </Box>
          <Box width={width / 4} alignItems="center">
            <Text fontSize="19" fontWeight="bold">
              {release}
            </Text>
            <Text color="grey">Release</Text>
          </Box>
          <Box width={width / 4} alignItems="center">
            <Text fontSize="19" fontWeight="bold">
              {language}
            </Text>
            <Text color="grey">Language</Text>
          </Box>
        </Box>
        <Box px="3" marginTop="5">
          <Text fontSize="15" color="grey">
            {des}
          </Text>
        </Box>
        <Box px="3" marginTop="5">
          <Text fontSize="15" color="grey">
            {richDescription}
          </Text>
        </Box>
      </ScrollView>
      <Box style={styles.buyNowContainer}>
        <Box
          backgroundColor="#f01c2c"
          flexDirection="row"
          py="2"
          px="3"
          borderRadius="10"
          style={styles.buyNowWrapper}
        >
          <Text color="white" fontSize="17" fontWeight="bold">
            {price} BDT{" "}
          </Text>

          {context.stateUser.user.userId ? (
            <TouchableOpacity
              style={styles.buyNowbtn}
              onPress={() => {
                props.addItemToCart(book);
              }}
            >
              <Text color="white" fontSize="17" fontWeight="bold">
                Add To Cart
              </Text>
            </TouchableOpacity>
          ) : (
            <TouchableOpacity
              style={styles.buyNowbtn}
              onPress={() => {
                Toast.show("Your Must Login", Toast.SHORT, [
                  "UIAlertController",
                ]);
              }}
            >
              <Text color="white" fontSize="17" fontWeight="bold">
                Add To Cart
              </Text>
            </TouchableOpacity>
          )}
        </Box>
      </Box>
    </Box>
  );
};

const mapToDispatchToProps = dispatch => {
  return {
    addItemToCart: product =>
      dispatch(actions.addToCart({ quantity: 1, product })),
  };
};

export default connect(null, mapToDispatchToProps)(SingleBook);

const styles = StyleSheet.create({
  imageContainer: {
    width: width,
    height: width / 1.2,
    justifyContent: "center",
    alignItems: "center",
  },
  buyNowContainer: {
    // position:'absolute',
    // bottom:0,
    // left:0,
    justifyContent: "center",
    alignItems: "center",
    width: width,
    // backgroundColor:'white',
    flexDirection: "row",
    height: "10%",
  },
  buyNowWrapper: {
    width: width / 1.3,
    justifyContent: "space-around",
  },
  buyNowbtn: {
    backgroundColor: "#eba7aa",
    borderRadius: 10,
    paddingHorizontal: 10,
    paddingVertical: 2,
  },
});
