import {useNavigation} from '@react-navigation/native';
import {Box, Text} from 'native-base';
import React from 'react';
import {Dimensions, StyleSheet, TouchableOpacity, View} from 'react-native';
import { Typography } from '../../../theme/Typography';
import PressableComponent from '../../ReUsebleComponent/PressableComponent';
import BookCard from './BookCard';

var {width} = Dimensions.get('window');

const BookList = props => {

  

  const {item} = props;
  
  const navigation = useNavigation();
  return (
    <TouchableOpacity
      style={{
        width: width / Typography.cardBoxTouchableWeight,
        justifyContent: 'center',
        alignItems: 'center',
      }}
      onPress={() => navigation.navigate('Single Book', {item:item})}>
      <Box  my="2"  style={{width: width / Typography.cardBoxWeight, backgroundColor: 'white'}}>
        <BookCard {...item} />
      </Box>
    </TouchableOpacity>
  );
};

export default BookList;

const styles = StyleSheet.create({});
