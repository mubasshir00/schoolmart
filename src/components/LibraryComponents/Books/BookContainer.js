import { useNavigation } from '@react-navigation/native';
import {Box, FlatList, Heading, ScrollView, Text} from 'native-base';
import React, {useContext, useEffect, useState} from 'react';
import {StyleSheet, View,TouchableOpacity } from 'react-native';
import AuthGlobal from '../../../context/store/AuthGlobal';
import BookList from './BookList';
import CategoryFilter from './CategoryFilter';
const data = require('../../../assets/LibraryAssets/Book.json');
const categories = require('../../../assets/LibraryAssets/categories.json')

const BookContainer = () => {
  const [books, setBooks] = useState([]);
  const [categories,setCategories] = useState([]);
  const [active,setActive]=useState();

  const navigation = useNavigation();

  

  useEffect(() => {
    setBooks(data);
    setCategories(categories);
    setActive(-1)
    return () => {
      setBooks([]);
      setCategories([]);
      setActive();
    };
  }, []);

  return (
    <Box background="white" borderRadius="5" marginX="2" py="3">
      <Box flexDirection="row" justifyContent="space-between">
      <Heading fontSize="15" px="2">
        Best Selling Last Week
      </Heading>
        {/* <TouchableOpacity
          onPress={() => navigation.navigate('All Book')}
          backgroundColor="red">
        <Text px="3">All Book</Text>
      </TouchableOpacity> */}
      </Box>

      <ScrollView
        bounces={true}
        horizontal={true}
      >
        {books.map(item => {
          return <BookList key={item.name + Math.random()} item={item} />;
        })}
      </ScrollView>
    </Box>
  );
};

export default BookContainer;

const styles = StyleSheet.create({});

{
  /* <FlatList
          numColumns={2}
          data={books}
          renderItem={({item}) => <BookList key={item.id} item={item} />}
          keyExtractor={item => item.writer}
        /> */
}
