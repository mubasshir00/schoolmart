import { useNavigation } from '@react-navigation/native'
import { Box, Text } from 'native-base'
import React from 'react'
import { Dimensions, StyleSheet, View } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import AntDesign from 'react-native-vector-icons/AntDesign'
const data = require('../../..//assets/LibraryAssets/Book.json');
// console.log(data);
const {width} = Dimensions.get('window')

const CategoryFilter = () => {
    const navigation = useNavigation();
    return (
        <Box marginTop="2" flex="1" flexWrap="wrap" alignItems="flex-start" flexDirection="row" justifyContent="center">
            <TouchableOpacity onPress={() => navigation.navigate('Academic Book',{data:data})}  style={styles.item}>
                <Box flexDirection="row" alignItems="center" justifyContent="space-between">
                    <Text fontWeight="bold" fontSize="15">Academic Book</Text>
                    <AntDesign name="right" size={20}/>
                </Box>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigation.navigate('Science & Tech')} style={styles.item}>
                <Box flexDirection="row" alignItems="center" justifyContent="space-between">
                    <Text fontSize="15" fontWeight="bold">Science & Tech</Text>
                    <AntDesign name="right" size={20} />
                </Box>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigation.navigate('Story & Novel')} style={styles.item}>
                <Box flexDirection="row" alignItems="center" justifyContent="space-between">
                    <Text fontSize="15" fontWeight="bold">Story & Novel</Text>
                    <AntDesign name="right" size={20}/>
                </Box>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigation.navigate('Drama & Poetry')} style={styles.item}>
                <Box flexDirection="row" alignItems="center" justifyContent="space-between" >
                    <Text fontSize="15" fontWeight="bold">Drama & Poetry</Text>
                    <AntDesign name="right" size={20}/>
                </Box>
            </TouchableOpacity>
        </Box>
    )
}

export default CategoryFilter

const styles = StyleSheet.create({
    item:{
        width: width/2.2,
        justifyContent:'center',
        backgroundColor:'white',
        marginVertical:5,
        marginHorizontal:3,
        borderRadius:6,
        height:40,
        borderWidth:2,
        borderColor:'white',
        paddingHorizontal:5,
    }
})
