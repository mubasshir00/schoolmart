import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import Category from './Category/Category'
import 'react-native-gesture-handler'
import { createStackNavigator } from '@react-navigation/stack'
import { NavigationContainer } from '@react-navigation/native'
import HomeScreen from '../screens/HomeScreen'

const CategoryStackNavigation = () => {
    const Stack = createStackNavigator()
    return (
        <Stack.Navigator>
            <Stack.Screen name="Home" component={HomeScreen}/>
        </Stack.Navigator>
    )
}

export default CategoryStackNavigation

const styles = StyleSheet.create({})
