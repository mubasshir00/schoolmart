import { Box, Image, ScrollView, Text } from "native-base";
import React, { useState, useEffect, useContext } from "react";
import { Dimensions, StyleSheet, View, TouchableOpacity } from "react-native";
import AntDesign from "react-native-vector-icons/AntDesign";
import { connect } from "react-redux";
import * as actions from "../../Redux/Actions/cartActions";
import Toast from "react-native-simple-toast";
import AuthGlobal from "../../context/store/AuthGlobal";
const { width, height } = Dimensions.get("screen");
const SingleBackPack = props => {
  const [backpack, setBackpack] = useState(props.route.params);
  console.log({ ...backpack.item });
  const context = useContext(AuthGlobal);
  const {
    brand,
    countInStock,
    totalrating,
    name,
    des,
    id,
    image,
    price,
    richDescription,
  } = { ...backpack.item };
  useEffect(() => {
    props.navigation.setOptions({
      title: name,
    });
  }, [name]);

  const ratingAv = 4;

  return (
    <Box background="info.50">
      <ScrollView
        contentContainerStyle={{
          alignItems: "center",
          height: "90%",
        }}
      >
        <Box style={styles.imageContainer}>
          <Image
            source={{ uri: image }}
            resizeMode="contain"
            style={styles.image}
            alt="name"
            borderRadius="5"
          />
          <Box marginTop="3" justifyContent="center" alignItems="center">
            <Box alignItems="center" flexDirection="row">
              <Text color="grey" fontWeight="bold">
                Model :{" "}
              </Text>
              <Text color="#f01c2c" fontWeight="bold" fontSize="20">
                {name}{" "}
              </Text>
              <Text fontWeight="bold" fontSize="15">
                ( {brand} )
              </Text>
            </Box>

            <Box alignItems="center" flexDirection="row">
              <Text color="grey" fontWeight="bold">
                In Stock :{" "}
              </Text>
              <Text fontWeight="bold" color="black" fontSize="16">
                {countInStock}
              </Text>
            </Box>

            <Box flexDirection="row">
              <Text>
                {Array(ratingAv)
                  .fill()
                  .map((_, i) => (
                    <Box px="0.5" key={i}>
                      <AntDesign name="star" color="orange" size={13} />
                    </Box>
                  ))}
              </Text>
              <Text>
                {Array(5 - ratingAv)
                  .fill()
                  .map((_, i) => (
                    <Box px="0.5" key={i}>
                      <AntDesign name="staro" size={13} />
                    </Box>
                  ))}
              </Text>
            </Box>

            <Box alignItems="center" flexDirection="row">
              <Text>Price : </Text>
              <Text fontSize="19" color="#f01c2c" fontWeight="bold">
                {price}
              </Text>
            </Box>

            {/* <Box alignItems='center' flexDirection='row'>
                            <Text fontSize='19' color='black' fontWeight='bold'>{richDescription}</Text>
                        </Box> */}

            <Box px="3" marginTop="5">
              <Text fontSize="15" color="grey">
                {richDescription}
              </Text>
            </Box>
          </Box>
        </Box>
      </ScrollView>

      <Box
        height="7%"
        marginTop="4"
        alignItems="center"
        marginX="3"
        background="#f01c2c"
        padding="2"
      >
        {context.stateUser.user.userId ? (
          <TouchableOpacity>
            <Text
              fontSize="20"
              fontWeight="bold"
              color="white"
              textAlign="center"
              onPress={() => {
                props.addItemToCart(backpack.item);
              }}
            >
              Add To Cart
            </Text>
          </TouchableOpacity>
        ) : (
          <TouchableOpacity>
            <Text
              fontSize="20"
              fontWeight="bold"
              color="white"
              textAlign="center"
              onPress={() => {
                Toast.show("Your Must Login", Toast.SHORT, [
                  "UIAlertController",
                ]);
              }}
            >
              Add To Cart
            </Text>
          </TouchableOpacity>
        )}
      </Box>
    </Box>
  );
};

const mapToDispatchToProps = dispatch => {
  return {
    addItemToCart: product =>
      dispatch(actions.addToCart({ quantity: 1, product })),
  };
};

export default connect(null, mapToDispatchToProps)(SingleBackPack);

const styles = StyleSheet.create({
  imageContainer: {
    width: width,
    marginTop: 5,
    // height: width / 1.2,
    justifyContent: "center",
    alignItems: "center",
  },
  image: {
    width: width / 1.5,
    height: width / 1.5,
  },
});
