import { useNavigation } from "@react-navigation/native";
import { Box, Text, Image, ScrollView } from "native-base";
import React, { useContext, useEffect, useState } from "react";
import { Dimensions, StyleSheet, TouchableOpacity, View } from "react-native";
import FontAweSome5 from "react-native-vector-icons/FontAwesome5";
import axios from "axios";
import Toast from "react-native-simple-toast";

// const data = require('../../assets/BackPack/backpack.json')

import * as actions from "../../Redux/Actions/cartActions";
import { connect } from "react-redux";
import baseURL from "../../assets/common/baseUrl";
import AuthGlobal from "../../context/store/AuthGlobal";
const { width, height } = Dimensions.get("screen");

const BackPackProducts = props => {
  const [data, setData] = useState([]);
  const context = useContext(AuthGlobal);

  const navigation = useNavigation();
  // console.log(props);

  useEffect(() => {
    axios
      .get(`${baseURL}/products?categoryName=BackPack`)
      .then(res => {
        console.log("res?.data", res?.data);
        setData(res?.data);
      })
      .catch(error => {
        console.log("Api Call Error");
      });
    return () => {
      setData([]);
    };
  }, []);

  return (
    <Box style={[styles.container]}>
      {data.map(item => {
        const { _id, name, image, price, brand } = item;
        console.log(_id);
        return (
          <TouchableOpacity
            onPress={() => navigation.navigate("SingleBackPack", { item })}
            style={[styles.item]}
            key={_id}
          >
            <Image
              source={{ uri: image }}
              resizeMode="cover"
              style={styles.image}
              alt="name"
              borderRadius="5"
            />
            <Box flexDirection="row" alignItems="center">
              <Text fontSize="15" fontWeight="bold" color="black">
                {name}
              </Text>
              <Text>{"  "}</Text>
              <Text color="grey" fontSize="13">
                {brand}
              </Text>
            </Box>
            <Box>
              <Text
                marginBottom="1"
                fontSize="13"
                fontWeight="bold"
                color="black"
              >
                {price} BDT
              </Text>
            </Box>
            {context.stateUser.user.userId ? (
              <TouchableOpacity
                onPress={() => {
                  props.addItemToCart(item);
                }}
              >
                <Box
                  backgroundColor="#f01c2c"
                  borderRadius="2"
                  // borderWidth="2"
                  flexDirection="row"
                  alignItems="center"
                  justifyContent="center"
                >
                  <FontAweSome5 name="cart-plus" color="white" />
                  <Text marginLeft="1" fontWeight="bold" color="white">
                    Add To Cart
                  </Text>
                </Box>
              </TouchableOpacity>
            ) : (
              <TouchableOpacity
                onPress={() => {
                  Toast.show("Your Must Login", Toast.SHORT, [
                    "UIAlertController",
                  ]);
                }}
              >
                <Box
                  backgroundColor="#f01c2c"
                  borderRadius="2"
                  // borderWidth="2"
                  flexDirection="row"
                  alignItems="center"
                  justifyContent="center"
                >
                  <FontAweSome5 name="cart-plus" color="white" />
                  <Text marginLeft="1" fontWeight="bold" color="white">
                    Add To Cart
                  </Text>
                </Box>
              </TouchableOpacity>
            )}
          </TouchableOpacity>
        );
      })}
    </Box>
  );
};

const mapDispatchToProps = dispatch => {
  return {
    addItemToCart: product =>
      dispatch(actions.addToCart({ quantity: 1, product })),
  };
};

export default connect(null, mapDispatchToProps)(BackPackProducts);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "row",
    flexWrap: "wrap",
    alignItems: "flex-start",
    justifyContent: "center",
  },
  item: {
    width: width / 2.3,
    // margin:5,
    marginHorizontal: 10,
    marginVertical: 7,
    padding: 5,
  },
  image: {
    width: width / 2.3,
    height: width / 2.4,
  },
});
