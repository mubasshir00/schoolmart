// import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
// import { NavigationContainer } from '@react-navigation/native';
// import React from 'react'
// import { StyleSheet, Text, View } from 'react-native'
// import HomeScreen from '../screens/HomeScreen';
// import InboxScreen from '../screens/InboxScreen';
// import TempScreen from '../screens/TempScreen';
// import AntIcon from 'react-native-vector-icons/AntDesign'

// const MyTab = () => {
//     const Tab = createBottomTabNavigator();

//     return (
//            <Tab.Navigator
//            screenOptions={({route})=>({
//                tabBarIcon:({focused,color,size}) =>{
//                    let iconName;
//                    if(route.name === 'Home'){
//                        iconName = 'home'
//                    } else if(route.name === "Inbox"){
//                        iconName = 'inbox'
//                    } else if(route.name === "Temp"){
//                        iconName = 'plus'
//                    }

//                    return <AntIcon name={iconName} size={size} color={color}/>
//                },
//                tabBarActiveTintColor:'white',
//                tabBarInactiveTintColor:'black',
//                tabBarStyle:{
//                    backgroundColor:'#EC653A',
//                }
//            }
//            )}
//            >
//                <Tab.Screen name="Home"
//                 component={HomeScreen}
//                 options={{headerShown:false}}/>
//                <Tab.Screen name="Temp"
//                 component={TempScreen}
//                 options={{headerShown:false}}/>
//                 <Tab.Screen name="Inbox"
//                 component={InboxScreen}
//                 options={{headerShown:false}}/>
//            </Tab.Navigator>
//     )
// }

// export default MyTab

// const styles = StyleSheet.create({})


// import React, { Component } from 'react'
// import { SafeAreaView, ScrollView, Text, TouchableOpacity, View } from 'react-native'
// import { RNCamera } from 'react-native-camera'
// import QRCodeScanner from 'react-native-qrcode-scanner'

// export class QrCodeScanner extends Component {
//     state = {
//         qr:""
//     }
//     onRead = e =>{
//         // this.setState({qr:e})
//         console.log(e);
//     }
//     render() {
//         return (
//            <SafeAreaView>
//                <ScrollView contentInsetAdjustmentBehavior="automatic">
//                     <View>
//             <QRCodeScanner
//                onRead={this.onRead}
//                flashMode={RNCamera.Constants.FlashMode.torch}
//               ref={(node) => { this.scanner = node }}
//         bottomContent={
//           <TouchableOpacity>
//             <Text >OK. Got it!</Text>
//           </TouchableOpacity>
//         }
//       />
//             </View>
//             <View>
//                 <Text>{this.state.qr}</Text>
//             </View>
//                </ScrollView>
//            </SafeAreaView>
//         )
//     }
// }

// export default QrCodeScanner

// import React, { Component } from 'react'
// import { SafeAreaView, ScrollView, Text, TouchableOpacity, View } from 'react-native'
// import { RNCamera } from 'react-native-camera'
// import QRCodeScanner from 'react-native-qrcode-scanner'

// export class QrCodeScanner extends Component {
//     // state = {
//     //     qr:""
//     // }
//     state = {
//     barcodes: [],
//   }

//   renderBarcode = ({ bounds, data }) => (
//     <React.Fragment key={data + bounds.origin.x}>
//       <View
//         style={{
//           borderWidth: 2,
//           borderRadius: 10,
//           position: 'absolute',
//           borderColor: '#F00',
//           justifyContent: 'center',
//           backgroundColor: 'rgba(255, 255, 255, 0.9)',
//           padding: 10,
//           ...bounds.size,
//           left: bounds.origin.x,
//           top: bounds.origin.y,
//         }}
//       >
//         <Text style={{
//           color: '#F00',
//           flex: 1,
//           position: 'absolute',
//           textAlign: 'center',
//           backgroundColor: 'transparent',
//         }}>{data}</Text>
//       </View>
//     </React.Fragment>
//   );

//     renderBarcodes = () => (
//     <View>
//       {this.state.barcodes.map(this.renderBarcode)}
//     </View>
//   );

//     onRead = e =>{
//         // this.setState({qr:e})
//         console.log(e);
//     }
//     barcodeRecognized = ({ barcodes }) => this.setState({ barcodes });

//     render() {
//         return (
//            <SafeAreaView>
//                <ScrollView contentInsetAdjustmentBehavior="automatic">
//                     <View>

//         <RNCamera
//            ref={ref => {
//             this.camera = ref;
//           }}
//           style={{
//             flex: 1,
//             width: '100%',
//           }}
//           onGoogleVisionBarcodesDetected={this.barcodeRecognized}
//         >
//                       {this.renderBarcodes()}

//        </RNCamera>
            
//             </View>
//             <View>
//                 <Text>{this.state.qr}</Text>
//             </View>
//                </ScrollView>
//            </SafeAreaView>
//         )
//     }
// }

// export default QrCodeScanner
