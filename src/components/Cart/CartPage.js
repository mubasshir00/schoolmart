import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { useCartContext } from '../../context/cart_context'

const CartPage = () => {
    const {cart} = useCartContext();
    if(cart.length<1){
        return (
            <View>
                <Text>Your Cart is Empty</Text>
            </View>
        )
    }
    return (
        <View>
            <Text>Cart Page</Text>
        </View>
    )
}

export default CartPage

const styles = StyleSheet.create({})
