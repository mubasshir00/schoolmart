import { useNavigation } from '@react-navigation/native'
import { Box, List, ScrollView, Text, Image } from 'native-base'
import React from 'react'
import { Dimensions, StyleSheet, View, TouchableOpacity } from 'react-native'
const { width } = Dimensions.get('window')
const SearchGifts = (props) => {
    const { giftsFiltered } = props
    const navigation = useNavigation()
    return (
    <ScrollView style={{ width: width }}>
    {
    giftsFiltered.length>0 ? (
    giftsFiltered.map((item)=>(
    <List px="4" key={item.id * Math.random() + Date.now().toString()}>
    <TouchableOpacity>
    <Box flexDirection="row" alignItems="center">
    <Box marginRight="2">

    <Image width="10" height="30" source={{ uri: item.image }} alt="image" />

    </Box>
    <Text>{item.name}</Text>
    </Box>
    </TouchableOpacity>
    </List>
    ))
    ) : (
        <Box>
            <Text style={{ alignSelf: 'center' }}>No Match Found</Text>
        </Box>  
    )
    }
    </ScrollView>
    )
}

export default SearchGifts

const styles = StyleSheet.create({})
