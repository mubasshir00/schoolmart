import { Box, Image, ScrollView, Text } from "native-base";
import React, { useEffect, useState } from "react";
import { Dimensions, StyleSheet, View, TouchableOpacity } from "react-native";
const { width, height } = Dimensions.get("screen");

const SingleGift = props => {
  const [gift, setGift] = useState(props.route.params);
  console.log({ ...gift.item });
  const { category, description, image, name, price, richDescription } = {
    ...gift.item,
  };

  useEffect(() => {
    props.navigation.setOptions({
      title: name,
    });
  }, [name]);

  return (
    <Box>
      <ScrollView height="90%">
        <Box marginTop="4" justifyContent="center" alignItems="center">
          <Image
            source={{ uri: image }}
            resizeMode="contain"
            style={styles.image}
            alt="name"
            borderRadius="2"
          />
          <Box marginTop="3" justifyContent="center" alignItems="center">
            <Box alignItems="center" flexDirection="row">
              <Text color="grey" fontWeight="bold">
                Name :{" "}
              </Text>
              <Text color="#f01c2c" fontWeight="bold" fontSize="20">
                {name}{" "}
              </Text>
            </Box>

            <Box alignItems="center" flexDirection="row">
              <Text>Price : </Text>
              <Text fontSize="19" color="#f01c2c" fontWeight="bold">
                {price}
              </Text>
            </Box>

            <Box paddingX="3">
              <Text>{description}</Text>
            </Box>
          </Box>
        </Box>
      </ScrollView>
      <Box marginTop="4" marginX="3" background="#f01c2c" padding="2">
        <TouchableOpacity>
          <Text
            fontSize="20"
            fontWeight="bold"
            color="white"
            textAlign="center"
          >
            Add To Cart
          </Text>
        </TouchableOpacity>
      </Box>
    </Box>
  );
};

export default SingleGift;

const styles = StyleSheet.create({
  image: {
    width: width / 1.2,
    height: width / 1.5,
  },
});
