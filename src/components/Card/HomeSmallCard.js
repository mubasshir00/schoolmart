import React from 'react';
import {
  FlatList,
  Image,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
} from 'react-native';
import { color } from '../../theme/Color';
import { Typography } from '../../theme/Typography';

const HomeSmallCard = ({Header, cardData}) => {
  // console.log(MyMart);
  return (
    <View style={[styles.cardContainer]}>
      <View style={[styles.headContainer]}>
        <Text style={[styles.headText]}>{Header}</Text>
      </View>
      <View style={[styles.itemContainer]}>
        <FlatList
          data={cardData}
          keyExtractor={item => item.id}
          horizontal
          renderItem={({item}) => (
            <TouchableOpacity style={[styles.singleItem]}>
              <View>
                <Image style={styles.image} source={item.image} />
              </View>
              <View>
                <Text style={styles.categoryText}>{item.title}</Text>
              </View>
            </TouchableOpacity>
          )}
        />
      </View>
    </View>
  );
};

export default HomeSmallCard;

const styles = StyleSheet.create({
  image: {
    width: Typography.iconImageWidth,
    height: Typography.iconImageHeight,
  },
  cardContainer: {
    backgroundColor: 'white',
    margin: 10,
  },
  headContainer: {
    borderBottomColor: '#E7E7E7',
    borderBottomWidth: 1,
    paddingVertical: 4,
  },
  itemContainer: {
    // paddingTop: 10,
    alignItems: 'center',
    padding:Typography.iconWithText_Padding
  },
  singleItem: {
    alignItems: 'center',
    paddingVertical: 1,
    // paddingHorizontal: 6,
    padding:Typography.iconWithText_Padding
  },
  headText: {
    paddingLeft: 10,
    color:color.textSecondaryColor,
    fontWeight:'bold'
  },
  categoryText:{
    fontSize:12
  }
});
