import {Box, ScrollView, Text, Image} from 'native-base';
import React, {useEffect, useState} from 'react';
import {Dimensions, StyleSheet, View} from 'react-native';
import Swiper from 'react-native-swiper';

var {width,height} = Dimensions.get('window');

const Carousel = () => {
  const [bannerData, setBannerData] = useState([]);

  useEffect(() => {
    setBannerData([
      "https://i.ibb.co/HhYg13W/Whats-App-Image-2022-03-21-at-8-32-06-PM.jpg",
      "https://i.pinimg.com/564x/f7/0b/96/f70b960462289600f89d48458c828343.jpg",
      "https://i.ibb.co/HhYg13W/Whats-App-Image-2022-03-21-at-8-32-06-PM.jpg",
    ]);
    return () => {
      setBannerData([]);
    };
  }, []);

  return (
    <Box>
      <View style={styles.container}>
        <View style={styles.swiper}>
          <Swiper
            style={{height: width / 3.9}}
            showButtons={false}
            autoplay={true}
            autoplayTimeout={2}>
            {bannerData.map(item => {
              return (
                <Image
                  key={item}
                  style={styles.imageBanner}
                  resizeMode="cover"
                  source={{uri: item}}
                  alt="text"
                />
              );
            })}
          </Swiper>
          {/* <View style={{height: 20}} /> */}
        </View>
      </View>
    </Box>
  );
};

export default Carousel;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  swiper: {
    width: width,
    alignItems: 'center',
    marginTop: 10,
    marginBottom:10,
  },
  imageBanner: {
    height: height / 3.9,
    width: width  ,
    borderRadius: 0,
  },
});
