import { useNavigation } from '@react-navigation/native'
import { Box, List, ScrollView, Text,Image } from 'native-base'
import React from 'react'
import { Dimensions, StyleSheet, View, TouchableOpacity } from 'react-native'
const {width} = Dimensions.get('window')
const SearchStationary = (props) => {
    const { stationariesFiltered} = props
    const navigation = useNavigation()
    console.log(stationariesFiltered);

    return (
        <ScrollView style={{width:width}}>
        {
            stationariesFiltered.length>0 ? (
                stationariesFiltered.map((item)=>(
                <List px="4" key={item.id*Math.random()+Date.now().toString()}>
                <TouchableOpacity >
                        <Box flexDirection="row" alignItems="center">
                        <Box marginRight="2">
                        
                        <Image width="10" height="30" source={{uri:item.image}} alt="image"/>

                        </Box>

                        <Box>
                        <Text>{item.productName}</Text>
                        </Box>

                        </Box>
                    </TouchableOpacity>
                    </List>
                ))
            ) : (
                <Box>
                    <Text style={{alignSelf:'center'}}>No Match Found</Text>
                </Box>
            )
        }
        </ScrollView>
    )
}

export default SearchStationary

const styles = StyleSheet.create({})
