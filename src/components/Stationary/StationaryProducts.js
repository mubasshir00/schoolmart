import { useNavigation } from "@react-navigation/native";
import { Box, Text, Image, ScrollView } from "native-base";
import React, { useContext, useEffect, useState } from "react";
import { Dimensions, StyleSheet, TouchableOpacity, View } from "react-native";
import FontAweSome5 from "react-native-vector-icons/FontAwesome5";
// const data = require('../../assets/Stationary/stationary.json')
import { connect } from "react-redux";
import * as actions from "../../Redux/Actions/cartActions";
import LibrarySlider from "../LibraryComponents/LibrarySlider";
import baseURL from "../../assets/common/baseUrl";
import axios from "axios";
import AuthGlobal from "../../context/store/AuthGlobal";
import Toast from "react-native-simple-toast";

const { width, height } = Dimensions.get("screen");

const StationaryProducts = props => {
  const navigation = useNavigation();
  const [data, setData] = useState([]);
  const context = useContext(AuthGlobal);

  useEffect(() => {
    axios
      .get(`${baseURL}/products?categoryName=Stationary`)
      .then(res => {
        setData(res?.data);
      })
      .catch(error => {
        console.log("Api Call Error");
      });

    return () => {
      setData([]);
    };
  }, []);
  return (
    <ScrollView>
      <LibrarySlider />
      <Box style={[styles.container]}>
        {data.map(item => {
          const { _id, productName, image, name, price } = item;
          return (
            <TouchableOpacity
              onPress={() => navigation.navigate("SingleBackPack", { item })}
              style={[styles.item]}
              key={_id}
            >
              <Image
                source={{ uri: image }}
                resizeMode="cover"
                style={styles.image}
                alt="name"
                borderRadius="5"
              />

              <Box flexDirection="column">
                <Box flexDirection="row" alignItems="center">
                  <Text fontSize="15" fontWeight="bold">
                    {name}
                  </Text>
                  <Text>{"  "}</Text>
                  <Text fontSize="12" fontWeight="bold" color="grey">
                    {productName}
                  </Text>
                </Box>

                <Text fontSize="15" fontWeight="bold">
                  {price} BDT
                </Text>
              </Box>
              {context.stateUser.user.userId ? (
                <TouchableOpacity
                  onPress={() => {
                    props.addItemToCart(item);
                  }}
                >
                  <Box
                    backgroundColor="#f01c2c"
                    borderRadius="2"
                    // borderWidth="2"
                    flexDirection="row"
                    alignItems="center"
                    justifyContent="center"
                  >
                    <FontAweSome5 name="cart-plus" color="white" />
                    <Text marginLeft="1" fontWeight="bold" color="white">
                      Add To Cart
                    </Text>
                  </Box>
                </TouchableOpacity>
              ) : (
                <TouchableOpacity
                  onPress={() => {
                    Toast.show("Your Must Login", Toast.SHORT, [
                      "UIAlertController",
                    ]);
                  }}
                >
                  <Box
                    backgroundColor="#f01c2c"
                    borderRadius="2"
                    // borderWidth="2"
                    flexDirection="row"
                    alignItems="center"
                    justifyContent="center"
                  >
                    <FontAweSome5 name="cart-plus" color="white" />
                    <Text marginLeft="1" fontWeight="bold" color="white">
                      Add To Cart
                    </Text>
                  </Box>
                </TouchableOpacity>
              )}
            </TouchableOpacity>
          );
        })}
      </Box>
    </ScrollView>
  );
};

const mapDispatchToProps = dispatch => {
  return {
    addItemToCart: product =>
      dispatch(
        actions.addToCart({
          quantity: 1,
          product,
        })
      ),
  };
};

export default connect(null, mapDispatchToProps)(StationaryProducts);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "row",
    flexWrap: "wrap",
    alignItems: "flex-start",
    justifyContent: "center",
    paddingBottom: 10,
  },
  item: {
    width: width / 2.3,
    // margin:5,
    marginHorizontal: 10,
    marginVertical: 7,
    padding: 5,
  },
  image: {
    width: width / 2.3,
    height: width / 2.4,
  },
});
