import { useNavigation } from '@react-navigation/native'
import axios from 'axios'
import { Box, Text,Input, Icon} from 'native-base'
import React, { useContext, useEffect, useState } from 'react'
import { Dimensions, StyleSheet, View } from 'react-native'

// const data = require('../../assets/Stationary/stationary.json')

import Ionicons from 'react-native-vector-icons/Ionicons'
import baseURL from '../../assets/common/baseUrl'
import AuthGlobal from '../../context/store/AuthGlobal'
import SearchStationary from './SearchStationary'
import StationaryProducts from './StationaryProducts'

const {width} = Dimensions.get('window')

const Stationary = () => {
    const navigation = useNavigation();


    const [data,setData] = useState([])
    const [stationaries, setStationaries] = useState([]);

    const [initialState, setInitialState] = useState([]);
    const [stationariesFiltered, setStationariesFiltered] = useState([]) 
    
    const [focus, setFocus] = useState()

    useEffect(() => {
        axios.get(`${baseURL}/products?categoryName=Stationary`)
        .then((res)=>{
            setData(res?.data)
        })
        .catch((error)=>{
            console.log('Api Call Error');
        })

        setFocus(false)
        setStationaries(data);
        setStationariesFiltered(data);
        return ()=> {
            setFocus();
            setStationariesFiltered([]);
            setStationaries([])
            setData([])
        };
    }, [])

    const searchStationary = (text) =>{
        setStationariesFiltered(
            stationaries.filter((i) => i.productName.toLowerCase().includes(text.toLowerCase()))
        )
    }

    const openList = () => {
        setFocus(true)
    }


    const onBlur = () =>{
        setFocus(false)
    }

    return (
        <Box background="info.50">
            <Box flexDirection="row" justifyContent="center" alignItems="center" marginBottom="0">
            </Box>
        <StationaryProducts/>
       </Box>
    )
}

export default Stationary

const styles = StyleSheet.create({})
