import { useNavigation } from '@react-navigation/native';
import { Box, Image, Text} from 'native-base';
import React from 'react'
import { Dimensions, StyleSheet, View, TouchableOpacity} from 'react-native'
import AntDesign from 'react-native-vector-icons/AntDesign';

const {width,height} = Dimensions.get('screen')

const ratingAv = 4;

const OfferedCouse = (props) => {
    // console.log(courseDate);
    const {courseDate} =props
    const navigation = useNavigation();
    return (
        <Box marginTop='4' paddingX='2'>
            {
                courseDate.map((item)=>{
                    const { courseName, image, instructor} = item
                    return(
                        <TouchableOpacity onPress={()=> navigation.navigate('SingleCourse',{item})} key={item.id}>
                            <Box style={styles.courseCard} >
                                <Image
                                    source={{ uri: image }}
                                    resizeMode="cover"
                                    style={styles.image}
                                    alt="name"
                                    borderRadius="5"
                                />
                                <Box style={styles.cardText}>
                                    <Text color='black' fontWeight='bold' fontSize='15'>{courseName?.length > 40 ? courseName.substring(0,40-5) + ' ...' : courseName}</Text>
                                    <Text color='grey'>{instructor}</Text>
                                    <Box flexDirection="row">
                                        <Text>
                                            {Array(ratingAv)
                                                .fill()
                                                .map((_, i) => (
                                                    <Box px="0.5" key={i}>
                                                        <AntDesign name="star" color="orange" size={13} />
                                                    </Box>
                                                ))}
                                        </Text>
                                        <Text>
                                            {Array(5 - ratingAv)
                                                .fill()
                                                .map((_, i) => (
                                                    <Box px="0.5" key={i}>
                                                        <AntDesign name="staro" size={13} />
                                                    </Box>
                                                ))}
                                        </Text>
                                    </Box>
                                </Box>
                            </Box>
                        </TouchableOpacity>
                    )
                })
            }
        </Box>
    )
}

export default OfferedCouse

const styles = StyleSheet.create({
    image: {
        width: width / 6,
        height: width / 6,
    },
    courseCard:{
        flexDirection:'row',
        marginVertical:3,
        alignItems:'center',
    },
    cardText:{
        marginLeft:10,
    }
})
