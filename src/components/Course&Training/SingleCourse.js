import { Box, Image, ScrollView, Text } from "native-base";
import React, { useEffect, useState } from "react";
import { Dimensions, StyleSheet, TouchableOpacity, View } from "react-native";
const { width, height } = Dimensions.get("screen");
const SingleCourse = props => {
  const [course, setCourse] = useState(props.route.params);
  console.log({ ...course.item });
  const {
    courseName,
    id,
    image,
    instructor,
    price,
    description,
    requirements,
  } = { ...course.item };
  console.log(requirements);

  useEffect(() => {
    props.navigation.setOptions({
      title: courseName,
    });
  }, [courseName]);

  return (
    <Box paddingX="3" flex="1" background="white">
      <ScrollView>
        <Box justifyContent="center" alignItems="center" marginTop="2">
          <Image
            source={{ uri: image }}
            resizeMode="cover"
            style={styles.image}
            alt="name"
            borderRadius="5"
            resizeMode="contain"
          />
          <Text fontSize="20" fontWeight="bold" textAlign="center">
            {courseName}
          </Text>
          <Text fontSize="15" color="grey">
            {description}
          </Text>
          <Box flexDirection="row" alignItems="center">
            <Text fontSize="18" fontWeight="bold">
              Instructor Name :{" "}
            </Text>
            <Text marginLeft="1">{instructor}</Text>
          </Box>

          <Text color="#f01c2c" fontWeight="bold">
            Price : {price}
          </Text>
          <Box>
            <Text fontSize="15" fontWeight="bold">
              Requirements
            </Text>
            <Box>
              {requirements.map(i => {
                return (
                  <Text lineHeight="25" paddingLeft="2" key={Math.random()}>
                    {" "}
                    * {i}
                  </Text>
                );
              })}
            </Box>
          </Box>
        </Box>
      </ScrollView>
      <Box marginTop="4" marginX="3" background="#f01c2c" padding="2">
        <TouchableOpacity>
          <Text
            fontSize="20"
            fontWeight="bold"
            color="white"
            textAlign="center"
          >
            Add To Cart
          </Text>
        </TouchableOpacity>
      </Box>
    </Box>
  );
};

export default SingleCourse;

const styles = StyleSheet.create({
  image: {
    width: width / 1.5,
    height: width / 2,
  },
});
