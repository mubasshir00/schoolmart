import { useNavigation } from "@react-navigation/native";
import { Box, Image, Text } from "native-base";
import React from "react";
import { Dimensions, StyleSheet, TouchableOpacity, View } from "react-native";
const { width, height } = Dimensions.get("screen");
const CourseTraining = () => {
  const navigation = useNavigation();
  return (
    <Box>
      <Image
        source={{
          uri: "https://image.freepik.com/free-vector/learning-concept-illustration_114360-6186.jpg",
        }}
        alt="banner"
        style={styles.image}
      />

      <Box style={styles.footerContent}>
        <Box padding="3">
          <Text fontSize="28" lineHeight="30" fontWeight="bold">
            Learn anytime and anywhere
          </Text>
          <Text fontSize="15" color="grey" marginTop="2">
            Learn new professions from the comfort of your home or at any time
            and place convenient for you
          </Text>
        </Box>

        <Box marginTop="4" marginX="3" background="#f01c2c" padding="2">
          <TouchableOpacity
            onPress={() => navigation.navigate("CourseTrainingCategory")}
          >
            <Text
              fontSize="20"
              fontWeight="bold"
              color="white"
              textAlign="center"
            >
              Let's Start
            </Text>
          </TouchableOpacity>
        </Box>
      </Box>
    </Box>
  );
};

export default CourseTraining;

const styles = StyleSheet.create({
  image: {
    width: width,
    height: width / 1.5,
  },
  footerContent: {},
});
