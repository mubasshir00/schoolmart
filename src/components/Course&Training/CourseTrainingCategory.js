import { Box, Icon, Input, ScrollView, Text } from 'native-base'
import React from 'react'
import { Dimensions, ImageBackground, StyleSheet, View } from 'react-native'
import  FontAwesome from 'react-native-vector-icons/FontAwesome'
import OfferedCouse from './OfferedCouse'
const {width,height} = Dimensions.get('screen')
const data = require('../../assets/Course&Training/courses.json')
const CourseTrainingCategory = () => {
    return (
        <ScrollView background='white'>
            <Box background='#E6F0F7' justifyContent='center' alignItems='center'
            marginX='3'
            >
                <Input
                fontSize='20'
                    w={{
                        base: '95%',
                        md: '25%'
                    }}
                    placeholder='Search'
                    InputLeftElement={
                        <Icon
                            as={<FontAwesome name="search" />}
                            size={5}
                            ml="2"
                            
                        
                        />
                    }
                />
            </Box>
            <Box >
                <ImageBackground
                    source={require('./background.jpg')}
                    style={styles.backgroundImage}
                >
                    <Box marginTop='30' justifyContent='center' alignItems='center'>
                        <Text style={styles.titleText}>What will we </Text>
                        <Text style={styles.titleText}>Study Today ? </Text>
                    </Box>
                </ImageBackground>
            </Box>
            <Box>
                <OfferedCouse courseDate={data}/>
            </Box>
        </ScrollView>
    )
}

export default CourseTrainingCategory

const styles = StyleSheet.create({
    backgroundImage:{
        width:width,
        height:width/2,
        opacity:30,
        zIndex:23,
    },
    titleText:{
        fontSize:20,
        fontWeight:'bold',
        color:'red'
    }
})
