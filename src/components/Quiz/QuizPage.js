import { useNavigation } from "@react-navigation/native";
import axios from "axios";
import { Box, Button, Center, Heading, HStack, Spinner, Text } from "native-base";
import React, { useEffect, useState } from "react";
import { StyleSheet, View, TouchableOpacity } from "react-native";
import baseURL from "../../assets/common/baseUrl";

const QuizPage = () => {
  const [questions, setQuestions] = useState();
  const [data, setData] = useState();
  const [count, setcount] = useState(0);
  const [rightAnswers,setRightAnswers] = useState([]);

  const navigation = useNavigation();


  useEffect(() => {
    axios
      .get(`${baseURL}/quiz/category_running_quiz?quiz_category=Bangla`)
      .then(res => {
        console.log("res?.data", res?.data);
        setData(res?.data);
        
      })
      .catch(error => {
        console.log("Api Call Error");
      });
    return () => {
      setData([]);
    };
  }, []);

  const changeQuiz = (e) =>{
    setcount(count+e);
    console.log(count);
    
  }

  setTimeout(()=>{ navigation.navigate("Profile");
  },8000)

  const rightAnswerUpdate = (e) =>{
    changeQuiz(1);
    rightAnswers.push(e)
  }
  console.log(rightAnswers);

  if(data){
  console.log(data[count]?.questions[count]);
  }

  if (!data) {
    return (
      <Center flex={1} px="3">
        <HStack space={2} alignItems="center">
          <Spinner
            size="lg"
            color="warning.500"
            accessibilityLabel="Loading Posts"
          />
          <Heading color="primary.500">Loading...</Heading>
         
        </HStack>
      </Center>
    );
  } else {
    if(count===10){
      return (
        <Center flex={1} px="3">
          <HStack space={2} alignItems="center">
            <Spinner
              size="lg"
              color="warning.500"
              accessibilityLabel="Loading Posts"
            />
            <Heading color="primary.500">Waiting.....</Heading>
            <Button onPress={() => navigation.navigate("Profile")}>Ok</Button>
          </HStack>
        </Center>
      );
    } else {
      return (
        <Box style={styles.container}>
          {data && (
            <Box style={styles.parent}>
              <Box style={styles.top}>
                <Text style={styles.question}>
                  {data[0]?.questions[count].question}
                </Text>
              </Box>

              {data[0]?.questions[count].options?.map((item, key) => {
                console.log(item);
                return (
                  <Box style={styles.options} key={key}>
                    <TouchableOpacity
                      style={{
                        width: "100%",
                        justifyContent: "center",
                        alignItems: "center",
                        height:'100%'
                      }}
                      onPress={e =>
                        rightAnswerUpdate({
                          index: count,
                          answer_index: key,
                        })
                      }
                    >
                      <Text style={{ color: "white", fontWeight: "bold",fontSize:20 }}>
                        {item}
                      </Text>
                    </TouchableOpacity>
                  </Box>
                );
              })}

              <Box style={styles.bottom}>
                <TouchableOpacity style={styles.button}>
                  <Text style={styles.buttonText}>Skip</Text>
                </TouchableOpacity>
                {/* <TouchableOpacity style={styles.button}>
                <Text style={styles.buttonText}>Skip</Text>
            </TouchableOpacity> */}
                <TouchableOpacity
                  style={styles.button}
                  onPress={() => changeQuiz(1)}
                >
                  <Text style={styles.buttonText}>Next</Text>
                </TouchableOpacity>
              </Box>
            </Box>
          )}
        </Box>
      );
    }
  }
};

export default QuizPage;

const styles = StyleSheet.create({
  container: {
    paddingTop: 40,
    paddingHorizontal: 20,
    height: "80%",
  },
  top: {
    marginVertical: 16,
  },
  options: {
    marginVertical: 16,
    flex: 1,
    backgroundColor:"#F01C2C",
    alignItems:'center',
    justifyContent:'center',
  },
  bottom: {
    // backgroundColor:"#f01c2c",
    marginBottom: 12,
    paddingVertical: 16,
    justifyContent: "space-between",
    flexDirection: "row",
  },
  button: {
    backgroundColor: "#f01c2c",
    padding: 12,
    borderRadius: 16,
    alignItems: "center",
    marginBottom: 30,
  },
  buttonText: {
    fontSize: 18,
    fontWeight: "600",
    color: "white",
  },
  question: {
    fontSize: 20,
  },
  option: {
    fontSize: 18,
    fontWeight: "500",
    color: "white",
  },
  optionButtom: {
    paddingVertical: 12,
    marginVertical: 6,
    backgroundColor: "#f01c2c",
    paddingHorizontal: 12,
    borderRadius: 12,
  },
  parent: {
    height: "100%",
  },
});
