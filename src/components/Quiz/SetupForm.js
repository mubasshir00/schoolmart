import React from "react";
import { Button, StyleSheet } from "react-native";
import { useGlobalContext } from "./context";
import { Picker } from "@react-native-picker/picker";
import { Box, Center, Text } from "native-base";
const SetupForm = () => {
  const { quiz, handleChange, handleSubmit, error } = useGlobalContext();
  // console.log(quiz.category);
  return (
    <Box flex={1}>
      <Box justifyContent="center" flex={1}>
        {/* category */}
        <Box>
          <Box style={styles.cateTextContainer}>
            <Text style={styles.cateText}>Category</Text>
          </Box>
          <Box style={styles.picker}>
            <Picker
              name="category"
              selectedValue={quiz.category}
              onValueChange={(itemValue, itemIndex) => handleChange(itemValue)}
            >
              <Picker.Item
                style={styles.pickerStyles}
                label="sports"
                value="sports"
              />
              <Picker.Item label="history" value="history" />
              <Picker.Item label="politics" value="politics" />
            </Picker>
          </Box>
        </Box>
        <Box style={styles.errorContainer}>
          {error && (
            <Text style={styles.errorText}>Can not Generate Questions</Text>
          )}
        </Box>
        <Box style={styles.btnContainer}>
          <Button
            style={styles.btnStyle}
            title="Start"
            onPress={handleSubmit}
          />
        </Box>
      </Box>
    </Box>
  );
};

export default SetupForm;

const styles = StyleSheet.create({
  errorContainer: {
    // marginTop: 10,
    // marginBottom: 10,
    padding: 10,
    borderRadius: 10,
    alignItems: "center",
  },
  errorText: {
    color: "red",
    fontSize: 18,
    fontWeight: "bold",
  },
  cateTextContainer: {
    alignItems: "center",
    justifyContent: "center",
    paddingBottom: 10,
  },
  cateText: {
    fontSize: 20,
    fontWeight: "bold",
  },
  picker: {
    backgroundColor: "#f01c2c",
    marginHorizontal: 30,
  },
  pickerStyles: {
    fontSize: 20,
  },
  btnContainer: {
    width: "100%",
    alignItems: "center",
    alignContent: "center",
  },
  btnStyle: {
    width: 400,
  },
});
