import React from 'react';
import {Button, StyleSheet, Text, View} from 'react-native';
import {useGlobalContext} from './context';

const Modal = () => {
  const {isModalOpen, closeModal, correct, questions, index} =
    useGlobalContext();
  return (
    <View
      style={`${
        isModalOpen ? styles.modalContainerOpen : styles.modalContainer
      }`}>
      <View>
        <View style={styles.modalContent}>
          <Text style={styles.modalText}>
            You Answered {((correct / questions.length) * 100).toFixed(0)}% of
            questions correctly{' '}
          </Text>
          <Button title="Play Again" onPress={closeModal} />
        </View>
      </View>
    </View>
  );
};

export default Modal;

const styles = StyleSheet.create({
  modalContainer: {
    backgroundColor: 'red',
    alignItems: 'center',
    justifyContent: 'center',
    opacity: 0,
    zIndex: -1,
    color: 'red',
  },
  modalContainerOpen: {
    position: 'absolute',
    top: 0,
    left: 0,
    backgroundColor: 'red',
    alignItems: 'center',
    justifyContent: 'center',
    opacity: 1,
    zIndex: 999,
    color: 'green',
  },
  modalContent: {
    textAlign: 'center',
    position: 'relative',
    alignItems: 'center',
    justifyContent: 'center',
    height: '100%',
    width: '100%',
  },
  modalText: {
    fontSize: 18,
    textAlign: 'center',
    margin: 10,
  },
});
