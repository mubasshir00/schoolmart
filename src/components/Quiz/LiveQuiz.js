import React from "react";
import { Button, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { useGlobalContext } from "../Quiz/context";
import Loading from "../Quiz/Loading";
import Modal from "../Quiz/Modal";
import SetupForm from "../Quiz/SetupForm";

const Quiz = () => {
  const {
    waiting,
    loading,
    questions,
    index,
    correct,
    nextQuestion,
    checkAnswer,
  } = useGlobalContext();
  if (waiting) {
    return <SetupForm />;
  }
  if (loading) {
    return <Loading />;
  }
  const { question, incorrect_answers, correct_answer } = questions[index];
  // console.log(question);
  console.log("index:", index);
  let answers = [...incorrect_answers];
  const tempIndex = Math.floor(Math.random() * 4);
  if (tempIndex === 3) {
    answers.push(correct_answer);
  } else {
    answers.push(answers[tempIndex]);
    answers[tempIndex] = correct_answer;
  }

  let content = (
    <>
      <View style={styles.quizContainer}>
        <View style={styles.container}>
          <Text style={styles.heading2}>{question}</Text>
          <View style={styles.btnContainer}>
            {answers.map((answer, index) => {
              return (
                <View key={index} style={styles.choicbtn}>
                  <TouchableOpacity
                    style={styles.answerbtn}
                    onPress={() => checkAnswer(correct_answer === answer)}
                    key={index}
                  >
                    <Text style={styles.answerText}>
                      {`${index + 1}.  ${answer}`}
                    </Text>
                  </TouchableOpacity>
                </View>
              );
            })}
          </View>
        </View>
        <Button
          style={styles.nextQuestion}
          onPress={nextQuestion}
          title="Next Question"
          color="#f01c2c"
        />
      </View>
      <View style={styles.correctContainer}>
        <Text style={styles.correctAnswer}>
          correct answers : {correct}/{index + 1}
        </Text>
      </View>
    </>
  );
  if (index === 9) {
    content = <Modal />;
  }
  return (
    <View style={styles.main}>
      <View style={styles.quiz}>{content}</View>
    </View>
  );
};

export default Quiz;

const styles = StyleSheet.create({
  main: {
    // flex: 1,
    justifyContent: "center",
    alignItems: "center",
    height: "100%",
  },
  quiz: {
    width: "100%",
    height: "100%",
    // marginHorizontal: 30,
    // backgroundColor: 'white',
    borderRadius: 10,
    // padding: 20,
    flex: 1,
  },
  quizContainer: {
    marginHorizontal: 20,
    padding: 10,
  },
  correctAnswer: {
    // fontSize: 17,
    marginBottom: 1,
    textAlign: "center",
    letterSpacing: 0.5,
    // textTransform: 'uppercase',

    fontSize: 20,
    color: "white",
    textTransform: "capitalize",
  },
  heading2: {
    marginTop: 10,
    marginBottom: 10,
    textAlign: "left",
    fontSize: 15,
    letterSpacing: 0.7,
  },
  btnContainer: {
    margin: 20,
  },
  answerbtn: {
    width: "100%",
    margin: 5,
    fontSize: 23,
    textAlign: "left",
  },
  nextQuestion: {},
  choicbtn: {
    margin: 4,
    backgroundColor: "white",
    color: "black",
  },
  answerText: {
    fontSize: 20,
    textAlign: "left",
  },
  correctContainer: {
    alignItems: "center",
    justifyContent: "center",
    // margin: 20,
    position: "absolute",
    bottom: 0,
    textAlign: "center",
    backgroundColor: "#f01c2c",
    width: "100%",
    // alignSelf: 'stretch',
    height: 50,
  },
  // correctAnswer: {
  //   fontSize: 20,
  //   color:'white',
  //   textTransform:'capitalize'
  // },
});

// style = { styles.answerbtn }
// onPress = {() => checkAnswer(correct_answer === answer)}
// key = { index }
// title = {`${index + 1}.  ${answer}`}
// color = "#00000"
