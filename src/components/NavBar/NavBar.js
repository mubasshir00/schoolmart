import { DrawerActions, useNavigation } from "@react-navigation/native";
import { Box, Icon, Input, Text } from "native-base";
import React, { useState } from "react";
import {
  Image,
  StyleSheet,
  View,
  TouchableOpacity,
  Dimensions,
} from "react-native";
import FontAweSome from "react-native-vector-icons/FontAwesome";
import Ionicons from "react-native-vector-icons/Ionicons";

const { width } = Dimensions.get("window");
const NavBar = () => {
  const navigation = useNavigation();
  const [focus, setFocus] = useState();

  const searchItem = text => {};

  const openList = () => {
    setFocus(true);
  };

  const onBlur = () => {
    setFocus(false);
  };

  return (
    <Box
      backgroundColor="#f01c2c"
      flexDirection="row"
      alignItems="center"
      justifyContent="space-between"
      paddingX="2"
    >
      <Box>
        <Image
          source={require("./../../assets/StudentWhite.png")}
          style={{
            width: 80,
            height: 40,
          }}
          
        />
      </Box>
      <Box>
        <TouchableOpacity
          onPress={() => navigation.navigate("Search Student Mart")}
        >
          <Box style={styles.searchBox}>
            <Ionicons size={30} color="white" name="search" borderRadius="0" />
          </Box>
        </TouchableOpacity>
        {/* <Input

          placeholder="Search"
          height="8"
          borderWidth="1"
          borderColor="#f01c2c"
          borderRadius="0"
          marginY="1.5"
          background="white"
          onFocus={openList}
          onChangeText={(text) => searchItem(text)}
          width={width / 1.3}

          InputRightElement={
            <Icon
              as={<Ionicons name="search" borderRadius="0" />}
              size={8}
              color="white"
              borderRadius="0"
              background="#f01c2c"
            />

          }
        /> */}
      </Box>
    </Box>
  );
};

export default NavBar;

const styles = StyleSheet.create({
  navBarContainer: {
    backgroundColor: "#f01c2c",
    // alignItems:'center',
    // paddingHorizontal:10
  },
  searchBox: {
    // height: 45,
    justifyContent: "center",
    alignItems: "center",
  },
  navBarWrapper: {
    flexDirection: "row",
    padding: 10,
    // alignItems:'center',
    // paddingHorizontal:20,
  },
  navBarContent: {
    paddingHorizontal: 10,
    alignItems: "center",
    alignContent: "center",
  },
  leftNavbar: {
    flexDirection: "row",
  },
  rightNavBar: {
    // width:'20%',
    alignItems: "flex-end",
    flexDirection: "row",
    // paddingRight: 20,
  },
  nameText: {
    // paddingTop: 10,
    fontSize: 17,
    fontWeight: "900",
    color: "white",
  },
});
