import { StyleSheet, View } from 'react-native'
import React, { useContext, useEffect, useState } from 'react'
import AuthGlobal from '../../context/store/AuthGlobal'
import axios from 'axios';
import baseURL from '../../assets/common/baseUrl';
import { Box, Center, Heading, HStack, ScrollView, Spinner, Text } from 'native-base'
import uuid from "react-native-uuid";

const Order = () => {
   const context = useContext(AuthGlobal);
   const [order,setOrder] = useState([]);
    console.log('context.stateUser.user.userId', context.stateUser.user.userId);
    useEffect(()=>{
        axios
        .get(`${baseURL}/orders/get/userorders/${context.stateUser.user.userId}`)
        .then((res)=>{
            setOrder(res?.data);
        })
        .catch((error)=>{
            console.log('Api Call Error');
        })
        return () =>{
            setOrder([])
        }
    },[])

  if(!order){
    return (
      <Center flex={1} px="3">
        <HStack space={2} alignItems='center'>
          <Spinner size="lg" color="warning.500" accessibilityLabel='Loading Posts' />
          <Heading color="primary.500">
            Loading
          </Heading>
        </HStack>
      </Center>
    )
  }
  console.log(order[0]);
  const { city, dateOrdered, id, orderItems, phone, shippingAddress1, shippingAddress2, status, totalPrice, zip
} = { ...order[0]};
  return (
    <ScrollView style={styles.container}>
      {
        status == "Pending" ? <Box style={styles.orderCard}>
          <Text style={styles.orderHead}>Current Order</Text>
          <Text>Order Id : {id}</Text>
          {
            orderItems?.map((i) => {
              console.log('iii', i);
              const { _id, quantity } = { ...i }
              const { name } = { ...i.product }
              return (
                <Box key={uuid.v4()} style={{ paddingVertical: 7 }}>
                  {/* <Text>{_id}</Text> */}
                  <Text style={styles.name}>{name}</Text>
                  <Text>Quantity : {quantity}</Text>
                </Box>
              );
            })
          }
          <Box>
            <Text style={styles.name}>Address </Text>
            <Box >
              <Text>Shipping Address : {shippingAddress1}</Text>
              <Text>Shipping Address : {shippingAddress2}</Text>
              <Text>{city} - {zip}</Text>
            </Box>
            <Box style={{ display: 'flex', flexDirection: 'row' }}>
              <Text>Total Price : </Text>
              <Text>{totalPrice}</Text>
            </Box>
            <Box style={{display:'flex',flexDirection:'row',paddingVertical:15}}>
              <Text style={{  fontSize: 20 }}>Status : </Text>
              <Text style={{ color: 'green', fontSize: 20}}>{status}</Text>
            </Box>
          </Box>
        </Box> : <Text>No Current Order </Text>
      }
      <Text style={styles.orderHead}>Order History</Text>
      {
        status != "Pending" ?
          <Box style={styles.orderCard}>
            {
              orderItems?.map((i) => {
                console.log('iii', i);
                const { _id, quantity } = { ...i }
                const { name } = { ...i.product }
                return (
                  <Box key={uuid.v4()} style={{ paddingVertical: 7 }}>
                    {/* <Text>{_id}</Text> */}
                    <Text style={styles.name}>{name}</Text>
                    <Text>Quantity : {quantity}</Text>
                  </Box>
                );
              })
            }
            <Box>
              <Text style={styles.name}>Address </Text>
              <Box >
                <Text>Shipping Address : {shippingAddress1}</Text>
                <Text>Shipping Address : {shippingAddress2}</Text>
                <Text>{city} - {zip}</Text>
              </Box>
              <Box style={{ display: 'flex', flexDirection: 'row' }}>
                <Text>Total Price : </Text>
                <Text>{totalPrice}</Text>
              </Box>
              <Box style={{ display: 'flex', flexDirection: 'row' }}>
                <Text>Status : </Text>
                <Text>{status}</Text>
              </Box>
            </Box>
          </Box>
        : <Text style={{textAlign:'center',fontSize:16}}>No History</Text>
      }
      
    </ScrollView>
  )
}

export default Order

const styles = StyleSheet.create({
  container:{
    backgroundColor:'white'
  },
  orderHead:{
    textAlign:'center',
    fontWeight:'bold',
    fontSize:30,
    paddingTop:20,
  },
  orderCard:{
    padding:10,
  },
  name:{
    fontSize:20,
    fontWeight:'bold'
  }
})