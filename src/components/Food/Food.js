import { useNavigation } from "@react-navigation/native";
import { Box, Icon, Image, Input, Text } from "native-base";
import React, { useState } from "react";
import {
  Dimensions,
  StyleSheet,
  View,
  TouchableOpacity,
  ScrollView,
} from "react-native";
import Ionicons from "react-native-vector-icons/Ionicons";
const { width } = Dimensions.get("window");

const offerImage =
  "https://i.pinimg.com/236x/24/36/be/2436bedc3a8d7f1059479c1b68bc0a46.jpg";

// https://i.pinimg.com/236x/43/e7/b8/43e7b8020e097a250addc7f4663fae84.jpg

// https://i.pinimg.com/236x/c2/53/4b/c2534b6364bee2b578d5bdedf942923c.jpg

const offerObject = [
  {
    id: "1",
    image:
      "https://i.pinimg.com/236x/43/e7/b8/43e7b8020e097a250addc7f4663fae84.jpg",
  },
  {
    id: "2",
    image:
      "https://i.pinimg.com/236x/c2/53/4b/c2534b6364bee2b578d5bdedf942923c.jpg",
  },
  {
    id: "3",
    image:
      "https://i.pinimg.com/236x/43/e7/b8/43e7b8020e097a250addc7f4663fae84.jpg",
  },
  {
    id: "4",
    image:
      "https://i.pinimg.com/236x/c2/53/4b/c2534b6364bee2b578d5bdedf942923c.jpg",
  },
];

const Food = () => {
  const navigation = useNavigation();

  const [focus, setFocus] = useState();

  const openList = () => {
    setFocus(true);
  };

  const onBlur = () => {
    setFocus(false);
  };

  return (
    <ScrollView background="info.50">
      <Box
        flexDirection="row"
        justifyContent="center"
        alignItems="center"
        marginBottom="0"
      ></Box>
      <Box marginX="0">
        <Image
          source={{ uri: offerImage }}
          resizeMode="cover"
          style={styles.image}
          alt="name"
          borderRadius="1"
        />
      </Box>

      <Box marginLeft="2">
        <Text fontSize="17" fontWeight="bold" color="black">
          Select Your Area
        </Text>
      </Box>

      <Box margin="2" style={styles.container}>
        <TouchableOpacity style={styles.item}>
          <Text fontSize="13" fontWeight="bold" color="white">
            Gulshan 1
          </Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.item}>
          <Text fontSize="13" fontWeight="bold" color="white">
            Gulshan 2
          </Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.item}>
          <Text fontSize="13" fontWeight="bold" color="white">
            Banani
          </Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.item}>
          <Text fontSize="13" fontWeight="bold" color="white">
            Basundhara R/A
          </Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.item}>
          <Text fontSize="13" fontWeight="bold" color="white">
            Mohammadpur
          </Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.item}>
          <Text fontSize="13" fontWeight="bold" color="white">
            Uttara
          </Text>
        </TouchableOpacity>
      </Box>

      <Box marginLeft="2" marginBottom="2">
        <Text fontSize="17" fontWeight="bold" color="black">
          Hot Deals
        </Text>
      </Box>

      <ScrollView horizontal={true}>
        <Box flexDirection="row">
          {offerObject.map(i => {
            return (
              <Image
                style={styles.offerI}
                source={{ uri: i.image }}
                key={i.id}
                alt="AA"
              />
            );
          })}
        </Box>
      </ScrollView>
    </ScrollView>
  );
};

export default Food;

const styles = StyleSheet.create({
  image: {
    width: width / 1,
    height: width / 2.4,
  },
  container: {
    flexDirection: "row",
    flexWrap: "wrap",
    alignItems: "center",
    justifyContent: "space-between",
  },
  item: {
    width: width / 3.2,
    textAlign: "center",
    alignItems: "center",
    backgroundColor: "#f01c2c",
    marginVertical: 2,
    paddingVertical: 2.3,
  },
  offerI: {
    width: width / 3,
    height: width / 2,
  },
  hotdeals: {
    flexDirection: "row",
  },
});
