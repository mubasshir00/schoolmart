import { StyleSheet, Text, View, Dimensions, TextInput, TouchableOpacity } from 'react-native'
import React, { useCallback, useContext, useState } from 'react'
import { useFocusEffect, useNavigation } from '@react-navigation/native'
import AuthGlobal from '../../context/store/AuthGlobal'
import AsyncStorage from '@react-native-async-storage/async-storage'
import axios from 'axios'
import baseURL from '../../assets/common/baseUrl'
import { Box, Image, ScrollView } from 'native-base'
const { width, height } = Dimensions.get('screen')


const PersonalInformation = () => {
  const navigation = useNavigation()
  const context = useContext(AuthGlobal);
  const [userProfile, setUserProfile] = useState()

    useFocusEffect(
        useCallback(() => {

            console.log('context.stateUser.user', context.stateUser.user);
            if (context.stateUser.isAuthenticated === false || context.stateUser.isAuthenticated == null) {
                navigation.navigate("Sign In")
            }
            AsyncStorage.getItem("jwt")
                .then(async (res) => {
                    await axios
                        .get(`${baseURL}/users/${context.stateUser.user.userId.toString()}`, {
                            headers: { Authorization: `Bearer ${res}` },
                        })
                        .then((user) => {
                            console.log(user);
                            setUserProfile(user?.data)
                        })
                })
                .catch((error) => console.log(error))

           

            return () => {
                setUserProfile()
            }

        }, [context.stateUser.isAuthenticated, context.stateUser.user, navigation])
    )

    console.log('userProfile', userProfile);
    const { profile_image, name, emailOrPhone, address, apartment, zip, city, country
    } = { ...userProfile }
    console.log(profile_image);
  return (
    <ScrollView height="100%" background='white'>
    <Box style={styles.container}>
        <Box>
                  <Box>
                      <Text style={{ fontSize: 19 ,textAlign:'center',marginBottom:9}}>{name}</Text>
                  </Box>

                  <Box style={styles.profileImageBox}>
                      
                      <Image
                          source={{ uri: profile_image }}
                          resizeMode="cover"
                          style={styles.image}
                          alt="name"
                          borderRadius="5"
                      />
                  </Box>
                  <Box style={styles.title}>
                      <Text style={{ fontSize: 15 }}>Name</Text>
                      <TextInput
                          placeholderTextColor="#444"
                          autoCapitalize='none'
                          keyboardType='email-address'
                          textContentType='emailAddress'
                          // autoFocus={true}
                          defaultValue={name}
                          style={styles.inputText}

                      />
                  </Box>
                  <Box style={styles.title}>
                      <Text>Email Or Phone</Text>
                      <TextInput
                          placeholderTextColor="#444"
                          autoCapitalize='none'
                          keyboardType='email-address'
                          textContentType='emailAddress'
                          // autoFocus={true}
                          defaultValue={emailOrPhone}
                          style={styles.inputText}

                      />
                  </Box>
                  <Box style={styles.title}>
                      <Text>Gender</Text>
                      <TextInput
                          placeholderTextColor="#444"
                          autoCapitalize='none'
                          keyboardType='email-address'
                          textContentType='emailAddress'
                          // autoFocus={true}
                          defaultValue="Male"
                          style={styles.inputText}

                      />
                  </Box>
                  <Box style={styles.title}>
                      <Text>Address</Text>
                      <TextInput
                          placeholderTextColor="#444"
                          autoCapitalize='none'
                          keyboardType='email-address'
                          textContentType='emailAddress'
                          // autoFocus={true}
                          defaultValue={address}
                          style={styles.inputText}
                      />
                  </Box>
                  <Box style={styles.title}>
                      <Text>Apartment</Text>
                      <TextInput
                          placeholderTextColor="#444"
                          autoCapitalize='none'
                          keyboardType='email-address'
                          textContentType='emailAddress'
                          // autoFocus={true}
                          defaultValue={apartment}
                          style={styles.inputText}

                      />
                  </Box>
                  <Box style={styles.title}>
                      <Text>City</Text>
                      <TextInput
                          placeholderTextColor="#444"
                          autoCapitalize='none'
                          keyboardType='email-address'
                          textContentType='emailAddress'
                          // autoFocus={true}
                          defaultValue={city}
                          style={styles.inputText}

                      />
                  </Box>
                  <Box style={styles.title}>
                      <Text>Country</Text>
                      <TextInput
                          placeholderTextColor="#444"
                          autoCapitalize='none'
                          keyboardType='email-address'
                          textContentType='emailAddress'
                          // autoFocus={true}
                          defaultValue={country}
                          style={styles.inputText}

                      />
                  </Box>
        </Box>
    </Box>
          <Box height="7%" marginTop='4' alignItems="center" marginX='12' background='#159FF4' padding='2'>
          <TouchableOpacity >
              <Text fontSize='20' fontWeight='bold' color='white' textAlign='center'
                  onPress={() => {
                      console.log('hh')
                  }}
              >Edit Info</Text>
          </TouchableOpacity>
          </Box>
   </ScrollView>
  )
}

export default PersonalInformation

const styles = StyleSheet.create({
    image: {
        width: width / 3.4,
        height: width / 3.4,
        borderRadius:width/3.4,
        borderWidth:1,
        borderColor:'black'
    },
    container:{
        display:'flex',
        alignItems:'center',
    },
    inputText:{
        fontSize:18,
        backgroundColor:'#EDEDED',
        width:width/1.3
    },
    title:{
        marginVertical:5
    },
    profileImageBox:{
        display:'flex',
        alignItems:'center',

    }
})