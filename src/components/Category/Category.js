import React, { useState } from "react";
import {
  FlatList,
  StyleSheet,
  TouchableOpacity,
  View,
  Image,
  Dimensions,
} from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import Ionicons from "react-native-vector-icons/Ionicons";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";
import school from "../../assets/categoryAssets/school.png";
import data from "../../../data";
import { useNavigation } from "@react-navigation/native";
import { Box, Text } from "native-base";
import { fontSizeConstant } from "../../theme/font";
import { color } from "../../theme/Color";
import { Typography } from "../../theme/Typography";
const { width, height } = Dimensions.get("screen");

// const categroyData = data.categoryData

const categroyData = data.categoryData.slice(0, 12);

const Category = () => {
  const navigation = useNavigation();

  const [backgroundBorder, setBackgroundBorder] = useState("white");

  const onPress = screenName => {
    // console.warn('Success');
    setBackgroundBorder("#f01c2c");
    navigation.navigate(screenName);
    // setBackgroundBorder('white')
  };

  const changeBorder = () => {};

  setTimeout(() => {
    setBackgroundBorder("white");
  }, 3000);

  return (
    <Box style={styles.container}>
      <FlatList
        data={categroyData}
        keyExtractor={item => item.id}
        renderItem={({ item }) => (
          <TouchableOpacity
            onPress={() => {
              onPress(item.screen);
              console.log(item.screen);
            }}
            style={[styles.cardContainer]}
          >
            <View style={{ alignItems: "center" }}>
              <Image
                style={styles.imageContainer}
                source={item.image}
                resizeMode={"cover"}
              />
              <Text fontSize={fontSizeConstant.categoryText}>{item.title}</Text>
            </View>
          </TouchableOpacity>
        )}
        numColumns={4}
      />
    </Box>
  );
};

export default Category;

const styles = StyleSheet.create({
  container: {
    backgroundColor: "white",
    alignItems: "center",
    borderBottomColor: "white",
    borderBottomWidth: 3,
    paddingBottom: 5,
    // paddingBottom:20,
    // paddingTop: 10,
    // paddingLeft:20,
    // paddingRight:20,
    // paddingHorizontal: 30,
  },
  cardContainer: {
    alignItems: "center",
    // paddingBottom: 10,
    width: 90,
    height: height / 9.4,
    padding: Typography.iconWithText_Padding,
    // borderColor: backgroundBorder,
  },
  imageContainer: {
    width: Typography.iconImageWidth,
    height: Typography.iconImageHeight,
  },
  categoryText: {
    color: color.textColor,
    fontWeight: "bold",
  },
});
