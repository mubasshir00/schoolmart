
import { Box } from 'native-base';
import React from 'react';
import {
    FlatList,
    Image,
    StyleSheet,
    Text,
    View,
    TouchableOpacity, Dimensions
} from 'react-native';
import { color } from '../../theme/Color';
import { Typography } from '../../theme/Typography';
const { width } = Dimensions.get('window')

const Offers = ({ Header, cardData }) => {
    console.log(cardData);
    return (
        <View style={[styles.cardContainer]}>
            <View style={[styles.headContainer]}>
                <Text style={[styles.headText]}>{Header}</Text>
            </View>
            <Box flexDirection='row'>
                {
                    cardData.map((i) => {
                        return (
                            <Image style={styles.offerI} source={{ uri: i.image }} key={i.id} alt='AA' />
                        )
                    })
                }
            </Box>
        </View>
    );
};

export default Offers;

const styles = StyleSheet.create({
    image: {
        width: Typography.iconImageWidth,
        height: Typography.iconImageHeight,
    },
    cardContainer: {
        flex:1,
        backgroundColor: 'white',
        // marginTop: 10,
    },
    headContainer: {
        borderBottomColor: '#E7E7E7',
        borderBottomWidth: 1,
        paddingBottom: 4,
    },
    itemContainer: {
        // paddingTop: 10,
        alignItems: 'center',
        padding: Typography.iconWithText_Padding
    },
    singleItem: {
        alignItems: 'center',
        paddingVertical: 1,
        // paddingHorizontal: 6,
        padding: Typography.iconWithText_Padding
    },
    headText: {
        paddingLeft: 10,
        color: color.textSecondaryColor,
        fontWeight: 'bold'
    },
    categoryText: {
        fontSize: 12
    },
    offerI: {
        width: width / 3,
        height: width / 2
    },
});
