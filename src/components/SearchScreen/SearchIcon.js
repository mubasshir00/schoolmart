import { useNavigation } from "@react-navigation/native";
import { Box, Text } from "native-base";
import React from "react";
import { StyleSheet, View, TouchableOpacity } from "react-native";
import FontAwesomeIcon from "react-native-vector-icons/FontAwesome5";
import Ionicons from "react-native-vector-icons/Ionicons";

const SearchIcon = () => {
  const navigation = useNavigation();
  return (
    <TouchableOpacity
      onPress={() => navigation.navigate("Search Student Mart")}
    >
      <Box alignItems="center" marginRight="2">
        <Ionicons size={30} color="#f01c2c" name="search" borderRadius="0" />
      </Box>
    </TouchableOpacity>
  );
};

export default SearchIcon;

const styles = StyleSheet.create({});
