import { StyleSheet, Text, View, Dimensions } from "react-native";
import React from "react";
import { Icon, Input } from "native-base";
import Ionicons from "react-native-vector-icons/Ionicons";
const { width, height } = Dimensions.get("window");
const SearchScreen = () => {
  return (
    <View style={styles.container}>
      <Input
        placeholder="Search In Student Mart"
        style={styles.inputStyle}
        InputRightElement={
          <Icon
            as={<Ionicons name="search" borderRadius="0" />}
            size={6}
            color="black"
            borderRadius="0"
            //   background="#f01c2c"
          />
        }
      />
    </View>
  );
};

export default SearchScreen;

const styles = StyleSheet.create({
  inputStyle: {
    // backgroundColor:'#f01c2c'
    width: width / 1.56,
    borderBottomColor: "black",
    alignItems: "center",
    justifyContent: "center",
    fontSize: 13,
  },
  container: {
    borderRadius: 0,
    borderLeftWidth: 0,
    borderRightWidth: 0,
    borderTopWidth: 0,
    borderWidth: 2,
    borderColor: "grey",
  },
});
