import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import { Box } from 'native-base'

const WhiteScreen = () => {
  return (
    <Box flex={1} background="white">

    </Box>
  )
}

export default WhiteScreen

const styles = StyleSheet.create({})