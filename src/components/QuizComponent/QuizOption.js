import { useNavigation } from "@react-navigation/native";
import React from "react";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import FontAweSome from "react-native-vector-icons/FontAwesome";

const QuizOption = () => {
  const navigation = useNavigation();
  return (
    <View style={styles.optionContainer}>
      {/* <TouchableOpacity style={styles.prevQuiz}>
        <FontAweSome style={styles.icon} name="star" color="#f01c2c" />
        <Text style={styles.prevText}>Archive Quiz</Text>
      </TouchableOpacity> */}

      <TouchableOpacity
        style={styles.liveQuiz}
        onPress={() => navigation.navigate("Live Quiz")}
      >
        <FontAweSome style={styles.icon} name="star" color="#f01c2c" />
        <Text style={styles.liveText}>Live Quiz</Text>
      </TouchableOpacity>
    </View>
  );
};

export default QuizOption;

const styles = StyleSheet.create({
  optionContainer: {
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    flex: 1,
  },
  prevQuiz: {
    backgroundColor: "white",
    fontSize: 24,
    width: "90%",
    alignItems: "center",
    justifyContent: "center",
    marginHorizontal: 40,
    marginVertical: 10,
    borderRadius: 20,
  },
  liveQuiz: {
    backgroundColor: "white",
    fontSize: 24,
    width: "90%",
    alignItems: "center",
    justifyContent: "center",
    marginHorizontal: 40,
    marginVertical: 10,
    borderRadius: 20,
  },
  prevText: {
    fontSize: 25,
    padding: 10,
    color: "#f01c2c",
    fontWeight: "bold",
    textAlign: "center",
  },
  liveText: {
    fontSize: 25,
    padding: 10,
    color: "#f01c2c",
    fontWeight: "bold",
    textAlign: "center",
  },
  icon: {
    fontSize: 100,
    paddingTop: 20,
  },
});
