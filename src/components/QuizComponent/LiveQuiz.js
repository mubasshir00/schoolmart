import { Box } from 'native-base'
import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import Category from './Category'

const LiveQuiz = () => {
  return (
    <Box style={styles.container}>
      <Category/>
    </Box>
  )
}

export default LiveQuiz

const styles = StyleSheet.create({
  container:{
    display:'flex',
    alignItems:'center',
    justifyContent:'center',
    marginVertical:'5%'
  }
})
