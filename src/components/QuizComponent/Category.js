import { Box, Pressable, ScrollView, Text } from "native-base";
import React, { useContext } from "react";
import {
  Dimensions,
  Image,
  ImageBackground,
  StyleSheet,
  TouchableOpacity,
  View,
} from "react-native";
import { useNavigation } from "@react-navigation/native";
import AuthGlobal from "../../context/store/AuthGlobal";
const { width, height } = Dimensions.get("screen");
const Category = () => {
  const navigation = useNavigation();
  const context = useContext(AuthGlobal);
  return (
    <ScrollView
      contentContainerStyle={{ justifyContent: "center", alignItems: "center" }}
    >
      {!context.stateUser.user.userId ? (
        <Box style={{alignItems:'center',justifyContent:'center'}}>
          <Text fontSize='20'>You Must Login</Text>
        </Box>
      ) : (
        <Box style={styles.container}>
          <Box style={styles.item}>
            <TouchableOpacity onPress={() => navigation.navigate("Quiz")}>
              <Box>
                <Text textAlign="center" fontSize={20} color="white">
                  বাংলা
                </Text>
              </Box>
            </TouchableOpacity>
          </Box>
          <Box style={styles.item}>
            <TouchableOpacity onPress={() => navigation.navigate("Quiz")}>
              <Box>
                <Text textAlign="center" fontSize={20} color="white">
                  বাংলা ব্যাকরণ
                </Text>
              </Box>
            </TouchableOpacity>
          </Box>
          <Box style={styles.item}>
            <TouchableOpacity onPress={() => navigation.navigate("Quiz")}>
              <Box>
                <Text textAlign="center" fontSize={20} color="white">
                  ইংরেজি
                </Text>
              </Box>
            </TouchableOpacity>
          </Box>
          <Box style={styles.item}>
            <TouchableOpacity onPress={() => navigation.navigate("Quiz")}>
              <Box>
                <Text textAlign="center" fontSize={20} color="white">
                  ইংরেজি ব্যাকরণ
                </Text>
              </Box>
            </TouchableOpacity>
          </Box>
          <Box style={styles.item}>
            <TouchableOpacity onPress={() => navigation.navigate("Quiz")}>
              <Box>
                <Text textAlign="center" fontSize={20} color="white">
                  ম্যাথমেটিক্স
                </Text>
              </Box>
            </TouchableOpacity>
          </Box>
          <Box style={styles.item}>
            <TouchableOpacity onPress={() => navigation.navigate("Quiz")}>
              <Box>
                <Text textAlign="center" fontSize={20} color="white">
                  খেলাধুলা
                </Text>
              </Box>
            </TouchableOpacity>
          </Box>
          <Box style={styles.item}>
            <TouchableOpacity onPress={() => navigation.navigate("Quiz")}>
              <Box>
                <Text textAlign="center" fontSize={20} color="white">
                  ইতিহাস
                </Text>
              </Box>
            </TouchableOpacity>
          </Box>
          <Box style={styles.item}>
            <TouchableOpacity onPress={() => navigation.navigate("Quiz")}>
              <Box>
                <Text textAlign="center" fontSize={20} color="white">
                  সাধারণ জ্ঞান
                </Text>
              </Box>
            </TouchableOpacity>
          </Box>

          <Box style={styles.item}>
            <TouchableOpacity onPress={() => navigation.navigate("Quiz")}>
              <Text textAlign="center" fontSize={20} color="white">
                জীববিজ্ঞান
              </Text>
            </TouchableOpacity>
          </Box>
          <Box style={styles.item}>
            <TouchableOpacity onPress={() => navigation.navigate("Quiz")}>
              <Text textAlign="center" fontSize={20} color="white">
                পদার্থবিজ্ঞান
              </Text>
            </TouchableOpacity>
          </Box>
          <Box style={styles.item}>
            <TouchableOpacity onPress={() => navigation.navigate("Quiz")}>
              <Text textAlign="center" fontSize={20} color="white">
                রসায়ন
              </Text>
            </TouchableOpacity>
          </Box>
          <Box style={styles.item}>
            <TouchableOpacity onPress={() => navigation.navigate("Quiz")}>
              <Text textAlign="center" fontSize={20} color="white">
                মহাকাশবিদ্যা{" "}
              </Text>
            </TouchableOpacity>
          </Box>
        </Box>
      )}
    </ScrollView>
  );
};

export default Category;

const styles = StyleSheet.create({
  categoryContainer: {
    alignItems: "center",
    justifyContent: "center",
    flex: 1,
  },
  container: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  item: {
    width: width / 1.2,
    backgroundColor: "#f01c2c",
    margin: 3,
    height: width / 8,
    justifyContent: "center",
  },
  imageBackground: {
    width: "100%",
    height: "100%",
    flex: 1,
  },
});
