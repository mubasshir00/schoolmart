import { StyleSheet, ScrollView, View, Image, Dimensions } from "react-native";
import React from "react";
import { Box, Text } from "native-base";
import LoginForm from "./LoginForm";
const { width, height } = Dimensions.get("window");
const LoginScreen = () => {
  return (
    <ScrollView contentContainerStyle={{ height: "100%" }}>
      <Box style={styles.container}>
        <Image
          source={require("./../assets/School-Mart.png")}
          style={{
            width: 160,
            height: 70,
            marginBottom: width / 12,

            // position:'absolute',
            // top:50,
          }}
        />
        <LoginForm />
      </Box>
    </ScrollView>
  );
};

export default LoginScreen;

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    flex: 1,
    // justifyContent: 'center',
    marginTop: width / 12,
  },
  logoContainer: {
    alignItems: "center",
    // marginTop:60,
  },
});
