import { StyleSheet, ScrollView, View, Image, Dimensions } from "react-native";
import React from "react";
import { Box, Text } from "native-base";
import SignUpForm from "./SignUpForm";
const { width, height } = Dimensions.get("window");

const SignupScreen = () => {
  return (
    <ScrollView
      contentContainerStyle={{ height: "100%", backgroundColor: "white" }}
    >
      <Box style={styles.container}>
        <Image
          source={require("./../assets/School-Mart.png")}
          style={{
            width: 100,
            height: 50,
            marginBottom: 10,
          }}
        />
        <SignUpForm />
      </Box>
    </ScrollView>
  );
};

export default SignupScreen;

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    flex: 1,
    // justifyContent: 'center',
    marginTop: width / 12,
  },
  logoContainer: {
    alignItems: "center",
  },
});
