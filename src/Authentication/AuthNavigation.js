import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import { useState } from 'react'
import { useEffect } from 'react'
import { firebase } from '@react-native-firebase/auth'

const AuthNavigation = () => {
  const [currentUser,setCurrentUser] = useState(null)

  const userHandler = user => {
      user ? setCurrentUser(user) : setCurrentUser(null)
  }

    useEffect(
        () => firebase.auth().onAuthStateChanged(user => userHandler(user)),
        []
    )

  return (
    <View>
      <Text>AuthNavigation</Text>
    </View>
  )
}

export default AuthNavigation

const styles = StyleSheet.create({})