import { Button, Pressable, StyleSheet, TextInput, View, Dimensions,TouchableOpacity } from 'react-native';
import React, { useContext, useEffect, useState } from 'react';
import { Box, Text} from 'native-base';
import { Formik } from 'formik';
import { useNavigation } from '@react-navigation/native';
const { width, height } = Dimensions.get('window')

// import {getAuth,signInWithEmailAndPassword} from 'firebase/auth'


//context 
import AuthGlobal from '../context/store/AuthGlobal';
import { loginUser } from '../context/actions/Auth.actions';

const LoginForm = () => {
  const context = useContext(AuthGlobal)
  const navigation = useNavigation();

  const [email,setEmail] = useState("");
  const [password,setPassword] = useState("");
  const [error,setError] = useState("");

  useEffect(()=>{
    if(context.stateUser.isAuthenticated === true){
      navigation.navigate("User Profile")
    }
  }, [context.stateUser.isAuthenticated, navigation])

  const handleSubmit = () =>{
    const user = {
      email,password
    };
    console.log(user);
    if(email === "" || password === ""){
      setError("Please fill in your kyc");
    } else {
      loginUser(user,context.dispatch)
    }
  }

  return (
    <Box style={styles.wrapper}>
      <>
        <Box style={{ marginBottom: 10 }}>
          <Text style={styles.loginHead}>Login</Text>
        </Box>
        <Box style={{ marginBottom: 20 }}>
          <Text style={styles.subHeaderText}>
            Enter registered email/mobile no. to Login
          </Text>
        </Box>
        <Box style={styles.inputField}>
          <TextInput
            placeholderTextColor="#444"
            placeholder="Phone Number or Email"
            autoCapitalize="none"
            keyboardType="email-address"
            textContentType="emailAddress"
            style={styles.inputText}
            // autoFocus={true}
            onChangeText={text => setEmail(text.toLowerCase())}
          />
        </Box>
        <Box style={styles.inputField}>
          <TextInput
            style={styles.inputText}
            placeholderTextColor="#000000"
            placeholder="Password"
            autoCapitalize="none"
            autoCorrect={false}
            secureTextEntry={true}
            textContentType="password"
            onChangeText={text => setPassword(text.toLowerCase())}
          />
        </Box>
        <Box
          style={{
            alignItems: "flex-end",
            marginBottom: 10,
            marginHorizontal: 10,
          }}
        >
          <Text style={{ color: "#6BB0F5" }}>Forgot Password?</Text>
        </Box>
        <TouchableOpacity
          titleSize={20}
          style={styles.button}
          // disabled={!isValid}
          onPress={() => handleSubmit()}
        >
          <Text style={styles.buttonText}>Log In</Text>
        </TouchableOpacity>
        <Box style={styles.signupContainer}>
          <Text>Don't have an account?</Text>
          <TouchableOpacity onPress={() => navigation.navigate("Sign Up")}>
            <Text style={{ color: "#6BB0F5", fontSize: 20 }}> Sign Up</Text>
          </TouchableOpacity>
        </Box>
      </>
      <Box style={styles.privacyContent}>
        <Text style={{ textAlign: "center" }}>
          By Signing in or Signing Up , you agree with out Terms & Conditions
          and Privacy Policy
        </Text>
      </Box>
    </Box>
  );
};

export default LoginForm;

const styles = StyleSheet.create({
  loginHead:{
    fontSize:20,
    textAlign:'center',
  },
  wrapper:{
    // marginTop:80,
  },
  inputText:{
    fontSize:17
  },
  subHeaderText:{
    textAlign:'center',
    marginBottom: 10,
    color:'grey'
  },
  inputField:{
    borderRadius:4,
    padding:6,
    backgroundColor:'#DDDDDD',
    marginBottom:width/20,
    // borderWidth:1,
    
    justifyContent:'center',
    marginHorizontal:10,
  },
  button:{
    backgroundColor:'#0096F6',
    alignItems:'center',
    justifyContent:'center',
    minHeight:42,
    borderRadius:4,
    marginHorizontal:10
  },
  buttonText:{
    fontWeight:'600',
    color:'#fff',
    fontSize:20
  },
  signupContainer:{
    flexDirection:'row',
    width:'100%',
    justifyContent:'center',
    marginTop:30,
    alignItems:'center'
  },
  privacyContent:{
    marginTop:10,
    color:'grey'
  }
});
