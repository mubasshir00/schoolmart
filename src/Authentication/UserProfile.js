import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  Dimensions,
} from "react-native";
import React, { useCallback, useContext, useState } from "react";
import AuthGlobal from "../context/store/AuthGlobal";
import { useFocusEffect, useNavigation } from "@react-navigation/native";
import AsyncStorage from "@react-native-async-storage/async-storage";
import axios from "axios";
import baseURL from "../assets/common/baseUrl";
import { Box, Icon, Image } from "native-base";
import { logoutUser } from "../context/actions/Auth.actions";
const { width, height } = Dimensions.get("screen");
import FontAwesome from "react-native-vector-icons/FontAwesome";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
const UserProfile = props => {
  const navigation = useNavigation();
  const context = useContext(AuthGlobal);
  const [userProfile, setUserProfile] = useState();
  const [orders, setOrders] = useState();
  console.log(props);
  useFocusEffect(
    useCallback(() => {
      console.log("context.stateUser.user", context.stateUser.user);
      if (
        context.stateUser.isAuthenticated === false ||
        context.stateUser.isAuthenticated == null
      ) {
        navigation.navigate("Sign In");
      }
      AsyncStorage.getItem("jwt")
        .then(async res => {
          await axios
            .get(
              `${baseURL}/users/${context.stateUser.user.userId.toString()}`,
              {
                headers: { Authorization: `Bearer ${res}` },
              }
            )
            .then(user => {
              console.log(user);
              setUserProfile(user?.data);
            });
        })
        .catch(error => console.log(error));

      // axios
      // .get(`${baseURL}/orders`)
      // .then((x)=>{
      //   const data = x.data;
      //   console.log(data);
      //   const userOrders = data.filter(
      //     (order) => order.user._id === context.stateUser.user.context.stateUser.user.userId.toString()
      //   );
      //   setOrders(userOrders);
      // })
      // .catch((error)=>console.log(error))

      return () => {
        setUserProfile();
        setOrders();
      };
    }, [context.stateUser.isAuthenticated, context.stateUser.user, navigation])
  );

  // console.log('userProfile', userProfile);
  // console.log('order',orders);

  const { name, emailOrPhone, address, Apartment, zip, city, country } = {
    ...userProfile,
  };

  //     < TouchableOpacity onPress = {() => {
  //   AsyncStorage.removeItem("jwt"),
  //     logoutUser(context.dispatch)
  // }}>
  //   <Text style={{ color: 'white', textAlign: 'center' }}>Sign Out</Text>
  //           </TouchableOpacity >

  // { "__v": 0, "_id": "6238cd2dabeb8194c65588ce", "address": "aa", "apartment": "bb", "city": "dd", "country": "BD", "emailOrPhone": "admin1221@gmail.com", "id": "6238cd2dabeb8194c65588ce", "isAdmin": true, "name": "aa", "passwordHash": "$2a$10$ISQDux32/7rsipiyQzJ8U.Ikh7oK/tEu74LKSGGsYRAWCcEvYU0KK", "zip": "ccc" }

  return (
    <Box style={styles.container}>
      <Box>
        <Text style={styles.heading}>My Account</Text>
      </Box>
      <Box>
        <Image
          source={{ uri: "https://randomuser.me/api/portraits/lego/1.jpg" }}
          resizeMode="cover"
          style={styles.image}
          alt="name"
          borderRadius="5"
        />
      </Box>
      <Box style={styles.nameContainer}>
        <Text style={styles.name}>{name}</Text>
      </Box>
      <Box background="info.50" width="100%">
        <Box style={styles.pointContainer}>
          <Image
            source={require("./../assets/School-Mart.png")}
            style={{
              width: 100,
              height: 50,
            }}
            alt="Profile Image"
          />
          <Box style={{ marginLeft: 5 }}>
            <Text style={{ fontSize: 19, color: "grey" }}>Available Point</Text>
            <Text style={{ fontSize: 19, color: "black", fontWeight: "bold" }}>
              0 Pts
            </Text>
          </Box>
        </Box>
      </Box>
      <Box style={styles.profileOption}>
        <Box style={styles.profileWrapper}>
          <TouchableOpacity
            onPress={() => navigation.navigate("Personal Information")}
            style={{ display: "flex", alignItems: "center" }}
          >
            <Box
              paddingTop="4"
              paddingLeft="2"
              backgroundColor="blue.500"
              style={styles.iconBox}
            >
              <Icon
                as={<FontAwesome name="user-o" />}
                size={10}
                color="white"
              />
            </Box>
            <Text style={{ fontSize: 18, fontWeight: "bold" }}>Profile</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => navigation.navigate("My Order")}
            style={{ display: "flex", alignItems: "center" }}
          >
            <Box
              paddingTop="4"
              backgroundColor="green.300"
              style={styles.iconBox}
            >
              <Icon
                as={<FontAwesome name="shopping-cart" />}
                size={10}
                color="white"
              />
            </Box>
            <Text style={{ fontSize: 18, fontWeight: "bold" }}>Order</Text>
          </TouchableOpacity>
          <TouchableOpacity style={{ display: "flex", alignItems: "center" }}>
            <Box
              paddingTop="4"
              backgroundColor="purple.300"
              style={styles.iconBox}
            >
              <Icon
                as={<FontAwesome name="heart-o" />}
                size={10}
                color="white"
              />
            </Box>
            <Text style={{ fontSize: 18, fontWeight: "bold" }}>Wishlist</Text>
          </TouchableOpacity>
        </Box>
      </Box>
      <Box style={styles.listContainer}>
        <TouchableOpacity style={styles.list}>
          <Box style={{ display: "flex", flexDirection: "row" }}>
            <Icon as={<FontAwesome name="list" />} size={7} color="black" />
            <Text style={styles.listText}>My List</Text>
          </Box>
          <Icon
            as={<FontAwesome name="angle-right" />}
            size={7}
            color="black"
          />
        </TouchableOpacity>
        <TouchableOpacity style={styles.list}>
          <Box style={{ display: "flex", flexDirection: "row" }}>
            <Icon
              as={<MaterialCommunityIcons name="android-messages" />}
              size={7}
            />
            <Text style={styles.listText}>My Review and Rating</Text>
          </Box>
          <Icon
            as={<FontAwesome name="angle-right" />}
            size={7}
            color="black"
          />
        </TouchableOpacity>
        <TouchableOpacity style={styles.list}>
          <Box style={{ display: "flex", flexDirection: "row" }}>
            <Icon as={<MaterialCommunityIcons name="logout" />} size={7} />
            <Text style={styles.listText}>Sign Out</Text>
          </Box>
          <Icon
            as={<FontAwesome name="angle-right" />}
            size={7}
            color="black"
          />
        </TouchableOpacity>
      </Box>
    </Box>
  );
};

export default UserProfile;

const styles = StyleSheet.create({
  container: {
    display: "flex",
    alignItems: "center",
    flexDirection: "column",
    backgroundColor: "white",
    height: "100%",
  },
  image: {
    width: width / 3.3,
    height: width / 3.4,
  },
  heading: {
    fontSize: 19,
    fontWeight: "bold",
    marginBottom: 10,
    marginTop: 10,
  },
  name: {
    fontSize: 19,
  },
  nameContainer: {
    marginVertical: 10,
  },
  pointContainer: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    marginVertical: 7,
    marginHorizontal: 10,
  },
  profileOption: {
    width: "100%",
    display: "flex",
    marginVertical: 10,
    paddingVertical: 10,
  },
  profileWrapper: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-around",
  },
  iconBox: {
    textAlign: "center",
    display: "flex",
    alignItems: "center",
    height: 74,
    width: 74,
    borderRadius: 74 / 2,
    marginBottom: 5,
  },
  listContainer: {
    width: width,
    paddingHorizontal: 10,
  },
  list: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    paddingVertical: 7,
    marginVertical: 7,
    paddingHorizontal: 7,
    justifyContent: "space-between",
    backgroundColor: "#ECF4F7",
  },
  listText: {
    fontSize: 18,
    marginLeft: 5,
  },
});
