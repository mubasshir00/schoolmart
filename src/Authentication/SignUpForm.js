import { StyleSheet, View, TouchableOpacity, TextInput, Dimensions } from 'react-native';
import React, { useState } from 'react';
import { Box, Text } from 'native-base';
import * as Yup from 'yup'
import { Formik } from 'formik';

import Toast from "react-native-toast-message"

import auth from '@react-native-firebase/auth';
import { useNavigation } from '@react-navigation/native';
import axios from 'axios';
import baseURL from '../assets/common/baseUrl';
const { width, height } = Dimensions.get('window')


const SignUpForm = () => {
  const navigation = useNavigation();
  const [emailOrPhone, setEmailOrPhone] = useState("");
  const [name,setName] = useState("");
  const [password,setPassword] = useState("");
  const [error,setError] = useState("");

  const register = () =>{
      if (emailOrPhone === "" || name === "" || password ===""){
          setError("Please fill information correctly")
      }

      let user = {
          name: name,
          emailOrPhone: emailOrPhone,
          password: password,
          isAdmin: false
      }

      axios
          .post(`${baseURL}/users/register`, user)
          .then((res) => {
              if (res.status == 200) {
                  Toast.show({
                      topOffset: 60,
                      type: "success",
                      text1: " Register Succeeded",
                      text2: "Please Login into your acount"
                  });
                  setTimeout(() => {
                      navigation.navigate("Sign In")
                  }, 500);
              }
          })
          .catch((error)=>{
              Toast.show({
                  topOffset: 60,
                  type: "error",
                  text1: "Something went wrong",
                  text2: "Please try again"
              })
          })
  }



  return (
    <Box style={styles.wrapper}>
      <>
        <Box style={{ marginBottom: 10 }}>
          <Text style={styles.signupHead}>Sign Up</Text>
        </Box>
        <Box style={styles.inputField}>
          <TextInput
            placeholderTextColor="#000000"
            placeholder="Phone Number or Email"
            autoCapitalize="none"
            keyboardType="email-address"
            textContentType="emailAddress"
            // autoFocus={true}
            style={styles.inputText}
            onChangeText={text => setEmailOrPhone(text.toLowerCase())}
          />
        </Box>

        <Box style={styles.inputField}>
          <TextInput
            placeholderTextColor="#000000"
            placeholder="Full Name"
            autoCapitalize="none"
            // autoFocus={true}
            style={styles.inputText}
            onChangeText={text => setName(text.toLowerCase())}
          />
        </Box>
        <Box style={styles.inputField}>
          <TextInput
            placeholderTextColor="#000000"
            placeholder="Password"
            autoCapitalize="none"
            autoCorrect={false}
            style={styles.inputText}
            secureTextEntry={true}
            textContentType="password"
            onChangeText={text => setPassword(text.toLowerCase())}
          />
        </Box>

        <TouchableOpacity
          titleSize={20}
          style={styles.button}
          // disabled={!isValid}
          onPress={() => register()}
        >
          <Text style={styles.buttonText}>Sign Up</Text>
        </TouchableOpacity>
        <Box style={styles.signupContainer}>
          <Text>Already have an account</Text>
          <TouchableOpacity onPress={() => navigation.navigate("Sign In")}>
            <Text style={{ color: "#6BB0F5", fontSize: 20 }}>Sign In</Text>
          </TouchableOpacity>
        </Box>
      </>
      <Box style={styles.privacyContent}>
        <Text style={{ textAlign: "center" }}>
          By Signing in or Signing Up , you agree with out Terms & Conditions
          and Privacy Policy
        </Text>
      </Box>
    </Box>
  );
};

export default SignUpForm;

const styles = StyleSheet.create({
    signupHead: {
        fontSize: 20,
        textAlign: 'center',
    },
    wrapper: {
        // marginTop:80,
    },
    inputField: {
        borderRadius: 4,
        padding: 6,
        backgroundColor: '#DDDDDD',
        marginBottom: width / 20,
        // borderWidth:1,

        justifyContent: 'center',
        marginHorizontal: 10,
    },
    button: {
        backgroundColor: '#0096F6',
        alignItems: 'center',
        justifyContent: 'center',
        minHeight: 42,
        borderRadius: 4,
        marginHorizontal: 10
    },
    buttonText: {
        fontWeight: '600',
        color: '#fff',
        fontSize: 20
    },
    signupContainer: {
        flexDirection: 'row',
        width: '100%',
        justifyContent: 'center',
        marginTop: 30,
        alignItems: 'center'
    },
    inputText:{
        fontSize: 17
    },
    privacyContent: {
        marginTop: 10,
        color: 'grey'
    }
});
