export const Typography = {
    "iconWithText_Padding" : 11,
    "iconImageWidth" : 33,
    "iconImageHeight" : 33,
    "cardBoxTouchableWeight": 2,
    "cardBoxWeight": 2.3
}