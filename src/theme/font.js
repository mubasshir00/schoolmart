export const fontSizeConstant = {
    "h1": 18,
    "h2" : 16,
    "h3" : 14,
    "normalText" : 15,
    "categoryText" : 12
}