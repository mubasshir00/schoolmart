import React, { useContext } from "react";
import { LOAD_PRODUCTS, SET_GRIDVIEW,SET_LISTVIEW,UPDATE_SORT,SORT_PRODUCTS,UPDATE_FILTERS,FILTER_PRODUCTS,CLEAR_FILTERS } from "../actions";
import reducer from '../reducers/filter_reducer';

const initialState = {}

const FilterContext = React.createContext()

export const FilterProvider = ({children}) =>{
    return(
    <FilterContext.Provider value="filter context">
        {children}
    </FilterContext.Provider>
    )
}

export const useFilterContext = () =>{
    return useContext(FilterContext)
}