import React, { useContext, useEffect, useReducer } from "react";
import { ADD_TO_CART ,REMOVE_CART_ITEM,TOGGLE_CART_ITEM_AMOUNT,CLEAR_CART,COUNT_CART_TOTALS} from "../actions";
import reducer from '../reducers/cart_reducer';
import  AsyncStorage  from "@react-native-async-storage/async-storage";

const initialState = {
    cart:[],
    total_items:0,
    total_price:0,
    shipping_fee:20
}

const CartContext = React.createContext()

export const CartProvider = ({children}) =>{
    const [state,dispatch] = useReducer(reducer,initialState)

    //add to cart
    const addToCart = (id,price,name,image) => {
        dispatch({type:ADD_TO_CART,payload:{id,price,name,image}})
    }

    //remove item
    const removeItem = (id) =>{

    }

    const toggleAmount = (id,value) =>{

    }

    const clearCart = () =>{

    }

    useEffect(() => {
       AsyncStorage.setItem('cart',JSON.stringify(state.cart))
    }, [state.cart])

    return(
        <CartContext.Provider value={{...state,addToCart,removeItem,toggleAmount,clearCart}}>{children}</CartContext.Provider>
    )
}

export const useCartContext = () =>{
    return useContext(CartContext)
}