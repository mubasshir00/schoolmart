import AsyncStorage from "@react-native-async-storage/async-storage";
import axios from "axios";
import jwt_decode from "jwt-decode";
import  Toast  from "react-native-toast-message";
import baseURL from "../../assets/common/baseUrl";

export const SET_CURRENT_USER = "SET_CURRENT_USER";

// export const loginUser = async (user, dispatch) => {
//     console.log('user', user);
//    await axios
//     .post(`${baseURL}/users/login`,{
//         "email" : user.email,
//         "password" : user.password
//     })
//     .then((res) => {
//         console.log(res);
//     })
//     .catch((err)=>{
//         console.log('err', err);
//     })
//     .then((data) => {
//         if (data) {
//                 const token = data.token;
//                 console.log(token);
//                 AsyncStorage.setItem("jwt", token)
//                 const decoded = jwt_decode(token)
//                 dispatch(setCurrentUser(decoded, user))
//             } else {
//                 logoutUser(dispatch)
//             }
//         })
//         .catch((err) => {
//             Toast.show({
//                 topOffset: 60,
//                 type: "error",
//                 text1: "Please Provide correct credentials",
//                 text2: ""
//             });
//             logoutUser(dispatch)
//         })
// }

export const loginUser = async (user,dispatch) =>{
    console.log('user',user);
   await fetch(`${baseURL}/users/login`,{
        method: "POST",
        body:JSON.stringify({
            emailOrPhone:user.email,
            password:user.password
        }),
        headers:{
            Accept: "application/json",
            "Content-Type": "application/json",
        },
    })
    .then((res)=>res.json())
    .then((data)=>{
        if(data){
            console.log(data);
            const token = data.token;
           
            AsyncStorage.setItem("jwt",token)
            const decoded = jwt_decode(token)
            dispatch(setCurrentUser(decoded,user))
        } else {
            logoutUser(dispatch)
        }
    })
    .catch((err)=>{
        console.log(err);
        Toast.show({
            topOffset:60,
            type:"error",
            text1:"Please Provide correct credentials",
            text2:""
        });
        logoutUser(dispatch)
    })
}

export const getUserProfile = (id) =>{
    fetch(`${baseURL}/users/${id}`,{
        method:'GET',
        body:user,
        headers:{
            Accept: "application/json",
            "Content-Type": "application/json",
        },
    })
    .then((res)=>res.json())
    .then((data)=>console.log(data))
}
export const logoutUser = (dispatch) =>{
    AsyncStorage.removeItem("jwt");
    dispatch(setCurrentUser({}))
}

export const setCurrentUser = (decoded,user) =>{
    return {
        type: SET_CURRENT_USER,
        payload: decoded,
        userProfile:user
    }
}