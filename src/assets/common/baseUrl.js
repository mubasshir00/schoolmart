import {Platform} from 'react-native'
let baseURL = "";

{
    Platform.OS == "android"
      ? (baseURL = "https://studentmarttest.herokuapp.com/api/v1")
      : (baseURL = "https://api.studentmart.net/api/v1");
}

export default baseURL

// baseURL = 'https://api.studentmart.net/api/v1' :
