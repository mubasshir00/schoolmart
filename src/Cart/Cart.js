import { Dimensions, StyleSheet, TouchableOpacity, View } from "react-native";
import { useNavigation } from "@react-navigation/native";
import React from "react";
import { connect } from "react-redux";
import { Box, Container, Image, Text } from "native-base";
import * as actions from "./../Redux/Actions/cartActions";
import { SwipeListView } from "react-native-swipe-list-view";
import CartItem from "./CartItem";
import Icon from "react-native-vector-icons/FontAwesome";
import NavBar from "../components/NavBar/NavBar";
import uuid from "react-native-uuid";

const { height, width } = Dimensions.get("window");

const Cart = props => {
  console.log(props.cartItems);
  const navigation = useNavigation();
  var total = 0;
  props.cartItems.forEach(cart => {
    return (total += cart.product.price);
  });
  return (
    <>
      <Box
        backgroundColor="white"
        flexDirection="row"
        alignItems="center"
        justifyContent="space-between"
        style={{
          height: 40,
        }}
      >
        <Box>
          <Text style={{ fontSize: 20, fontWeight: "bold", marginLeft: 20 }}>
            Cart
          </Text>
        </Box>
      </Box>
      {props.cartItems.length ? (
        <Box background="white" flex="1">
          <Box>
            <SwipeListView
              data={props.cartItems}
              renderItem={data => <CartItem item={data} />}
              renderHiddenItem={data => (
                <View style={styles.hiddenContainer}>
                  <TouchableOpacity
                    onPress={() => props.removeFromCart(data.item)}
                    style={styles.hiddenButton}
                  >
                    <Icon name="trash" color={"black"} size={20} />
                  </TouchableOpacity>
                </View>
              )}
              disableRightSwipe={true}
              previewOpenDelay={3000}
              friction={1000}
              leftOpenValue={75}
              tension={40}
              stopLeftSwipe={75}
              rightOpenValue={-75}
            />
          </Box>
          <View style={styles.bottomContainer}>
            <Box style={styles.price}>
              <Text>${total.toFixed(2)}</Text>
            </Box>
            <Box>
              <TouchableOpacity onPress={() => props.clearCart()}>
                <Text>Clear Cart</Text>
              </TouchableOpacity>
            </Box>
            <Box>
              <TouchableOpacity
                onPress={() => props.navigation.navigate("Checkout")}
              >
                <Text>Checkout</Text>
              </TouchableOpacity>
            </Box>
          </View>
        </Box>
      ) : (
        <Box style={styles.emptyContainer}>
          <Text fontSize="15" fontWeight="bold">
            Your Cart is Empty
          </Text>
        </Box>
      )}
    </>
  );
};

const mapStateToProps = state => {
  const { cartItems } = state;
  return {
    cartItems: cartItems,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    clearCart: () => dispatch(actions.clearCart()),
    removeFromCart: item => dispatch(actions.removeFromCart(item)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Cart);

const styles = StyleSheet.create({
  emptyContainer: {
    height: height,
    alignItems: "center",
    justifyContent: "center",
    flex: 1,
  },
  body: {
    margin: 10,
    alignItems: "center",
    flexDirection: "row",
  },
  bottomContainer: {
    marginTop: 5,
    padding: 4,
    width: width,
    flexDirection: "row",
    position: "absolute",
    bottom: 0,
    left: 0,
    backgroundColor: "white",
    elevation: 20,
    justifyContent: "space-between",
  },
  price: {
    fontSize: 18,
    // margin:20,
    color: "red",
  },
  hiddenContainer: {
    flex: 1,
    justifyContent: "flex-end",
    flexDirection: "row",
  },
  hiddenButton: {
    backgroundColor: "red",
    justifyContent: "center",
    alignItems: "flex-end",
    paddingRight: 25,
    // height:70,
    width: width / 1.2,
  },
});
