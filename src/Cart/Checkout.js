import {
  Button,
  StyleSheet,
  Text,
  TextInput,
  View,
  Dimensions,
} from "react-native";
import React, { useEffect, useState } from "react";
import Input from "../components/ReUsebleComponent/Form/Input";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { connect } from "react-redux";
import FormContainer from "../components/ReUsebleComponent/Form/FormContainer";
import { useNavigation } from "@react-navigation/native";
import { Box } from "native-base";
const { width, height } = Dimensions.get("window");
import uuid from "react-native-uuid";

const Checkout = props => {
  const [orderItems, setOrderItems] = useState();
  const [address, setAddress] = useState();
  const [address2, setAddress2] = useState();
  const [city, setCity] = useState();
  const [zip, setZip] = useState();
  const [country, setCountry] = useState();
  const [phone, setPhone] = useState();


  const navigation = useNavigation();

  useEffect(() => {
    setOrderItems(props.cartItems);
    return () => {
      setOrderItems();
    };
  }, [props.cartItems]);

  const checkOutTo = () => {
    let order = {
      city,
      dateOrdered: Date.now(),
      orderItems,
      phone,
      shippingAddress1: address,
      shippingAddress2: address2,
      zip,
    };
    console.log('order',order);
    // order {"city": "Tt", "dateOrdered": 1652822805768, "orderItems": [{"product": [Object], "quantity": 1}, {"product": [Object], "quantity": 1}, {"product": [Object], "quantity": 1}], "phone": "22", "shippingAddress1": "Gg", "shippingAddress2": "Gg", "zip": "55"}

    navigation.navigate("Payment", { order: order });
  };

  return (
    
      <FormContainer>
        <TextInput
          width={width / 1.3}
          style={{ paddingLeft: 10, marginTop: 10 }}
          placeholderTextColor="#000000"
          backgroundColor="#DDDDDD"
          placeholder={"Phone"}
          keyboardType={"numeric"}
          onChangeText={text => setPhone(text)}
        />

        <TextInput
          width={width / 1.3}
          style={{ paddingLeft: 10, marginTop: 10 }}
          placeholderTextColor="#000000"
          backgroundColor="#DDDDDD"
          placeholder={"Shipping Address"}
          onChangeText={text => setAddress(text)}
        />
        <TextInput
          width={width / 1.3}
          style={{ paddingLeft: 10, marginTop: 10 }}
          placeholderTextColor="#000000"
          backgroundColor="#DDDDDD"
          placeholder={"Shipping Address 2"}
          onChangeText={text => setAddress2(text)}
        />
        <TextInput
          width={width / 1.3}
          style={{ paddingLeft: 10, marginTop: 10 }}
          placeholderTextColor="#000000"
          backgroundColor="#DDDDDD"
          placeholder={"City"}
          onChangeText={text => setCity(text)}
        />
        <TextInput
          width={width / 1.3}
          style={{ paddingLeft: 10, marginTop: 10 }}
          placeholderTextColor="#000000"
          backgroundColor="#DDDDDD"
          placeholder={"Postal Code"}
          keyboardType={"numeric"}
          onChangeText={text => setZip(text)}
        />
        <View style={{ width: "80%", alignItems: "center", marginTop: 20 }}>
          <Button onPress={() => checkOutTo()} title="Confirm" />
        </View>
       
      </FormContainer>
    
  );
};

const mapStateToProps = state => {
  const { cartItems } = state;
  return {
    cartItems: cartItems,
  };
};

export default connect(mapStateToProps)(Checkout);

const styles = StyleSheet.create({
  inputField: {
    borderRadius: 4,
    padding: 6,
    backgroundColor: "#DDDDDD",
    marginBottom: width / 20,
    // borderWidth:1,

    justifyContent: "center",
    marginHorizontal: 10,
    width:width/1.3
  },
});
