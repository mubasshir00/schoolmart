import { StyleSheet, View } from 'react-native';
import React, { useState } from 'react';
import { Box, Image, List, Text } from 'native-base';

const CartItem = (props) => {
  const {name,image,price} = props.item.item.product
  const [quantity,setQuantity] = useState(props.item.item.quantity)
//   console.log('daaata',data);
  return (
      <Box style={styles.listItem} key={Math.random()} avatar>
          <Box>
              <Image width="10" height="10" source={{uri:image}} alt='aa'/>
          </Box>
          <Box style={styles.body}>
              <Box>
                  <Text fontWeight="bold" color="grey">{name}</Text>
                  <Text>${price}</Text>
              </Box>
          </Box>
      </Box>
  );
};

export default CartItem;

const styles = StyleSheet.create({
    listItem:{
        alignItems:'center',
        backgroundColor:'white',
        // justifyContent:'center',
        flexDirection:'row',
        paddingHorizontal:10,
        borderColor:'grey'
    },
    body:{
        margin:10,
        alignItems:'center',
        flexDirection:'row'
    }
});
