import { Button, StyleSheet, Text, View, TouchableOpacity } from "react-native";
import React, { useEffect, useState } from "react";
import { Box, Radio } from "native-base";
import { Picker } from "@react-native-picker/picker";
import { useNavigation } from "@react-navigation/native";

const methods = [
  { name: "Cash on Delivery", value: 1 },
  { name: "Bank Transfer", value: 2 },
  { name: "Card Payment", value: 3 },
];

const paymentCards = [
  { name: "Wallet", value: 1 },
  { name: "Visa", value: 2 },
  { name: "MasterCard", value: 3 },
  { name: "Other", value: 4 },
];

const Payment = props => {
  const order = props.route.params;

  const [selected, setSelected] = useState();
  const [card, setCard] = useState();

  const navigation = useNavigation();
  // console.log('selected',selected);

  const confirmOrder = () => {
    console.log("selected", selected);
    // order.paymentMethod = {
    //     selected : selected,
    //     name: methods[selected - 1].name
    // }

    navigation.navigate("Confirm", { order });
    const data = {
      status: "Pending",
      orderItems: [
        {
          product: "6249da68109914528b56f505",
          quantity: 2,
        },
        {
          product: "6249da6d109914528b56f508",
          quantity: 4,
        },
      ],
      shippingAddress1: "sazovicka",
      shippingAddress2: "zlicin",
      city: "Praha",
      zip: "15521",
      country: "Czech Republic",
      phone: 702241333,
      totalPrice: 350,
      user: "6235fce8dcb2b658f28b9aa2",
      __v: 0,
    };
    // console.log(data);
  };

  return (
    <Box style={styles.container}>
      <Box>
        <Text>Currently we accept cash on</Text>
        <View style={{ marginTop: 60, alignSelf: "center" }}>
          <Button
            title={"Confirm"}
            onPress={() => navigation.navigate("Confirm", { order })}
          />
        </View>
      </Box>
      {/* <Box>
        <Box>
          <Text style={styles.fontHeader}>Choose your payment method</Text>
        </Box>
      </Box>
      <Box>
        <Radio.Group
          value={selected || ""}
          onChange={nextValue => {
            setSelected(nextValue);
          }}
        >
          {methods.map((item, index) => {
            return (
              <Radio
                style={{ marginVertical: 5 }}
                key={item.name}
                value={item.value || ""}
              >
                {item.name}
              </Radio>
            );
          })}
        </Radio.Group>

        {selected === 3 ? (
          <Box style={styles.picker}>
            <Picker
              mode="dropdown"
              selectedValue={card}
              onValueChange={x => setCard(x)}
            >
              {paymentCards.map((c, index) => {
                return (
                  <Picker.Item key={c.name} label={c.name} value={c.name} />
                );
              })}
            </Picker>
          </Box>
        ) : null}
        <View style={{ marginTop: 60, alignSelf: "center" }}>
          <Button title={"Confirm"} onPress={() => navigation.navigate("Confirm",{order})} />
        </View>
      </Box> */}
    </Box>
  );
};

export default Payment;

const styles = StyleSheet.create({
  picker: {
    backgroundColor: "#f01c2c",
    marginHorizontal: 30,
    width: 190,
    marginTop: 5,
  },
  pickerStyles: {
    fontSize: 20,
  },
  fontHeader: {
    fontSize: 20,
    marginBottom: 10,
  },
  container: {
    alignItems: "center",
    justifyContent: "center",
    marginTop: 20,
  },
});
