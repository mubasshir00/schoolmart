import { ScrollView, StyleSheet, Text, View, TextInput } from "react-native";
import React, { useEffect, useState, useContext } from 'react';
import { Box, Button, Image } from 'native-base';
import AuthGlobal from '../context/store/AuthGlobal'
import axios from 'axios';
import baseURL from '../assets/common/baseUrl';
import { useNavigation } from '@react-navigation/native';
import uuid from "react-native-uuid";

const Confirm = (props) => {
  const finalOrder = props.route.params
  // console.log(props.route);
  console.log('final',{ ...finalOrder?.order});
  const { city, dateOrdered, orderItems, phone, shippingAddress1, shippingAddress2, paymentMethod, zip
} = { ...finalOrder?.order?.order}
//{"order": {"city": "c", "dateOrdered": 1650710923044, "orderItems": [[Object], [Object]], "phone": "56789", "shippingAddress1": "a", "shippingAddress2": "b", "zip": "4321"}, "paymentMethod": {"name": "Cash on Delivery", "selected": 1}}
  console.log('orderItems', orderItems);

    const navigation = useNavigation();



  const [productUpdate,setProductUpdate] = useState();

  const context = useContext(AuthGlobal)

  // console.log(context.stateUser.user);
  // console.log(props.cartItems);

  const ConfirmOrder = props.route.params
  console.log("confirm", ConfirmOrder?.order?.order?.orderItems);

  const orderItems_data = []

  const tempData = ConfirmOrder?.order?.order?.orderItems.map(i => {
    console.log(i.product._id);
    return orderItems_data.push({
      product: i?.product?._id,
      quantity: i?.quantity,
    });
  });
  console.log('tempData', orderItems_data);

  // [{ "product": { "__v": 0, "_id": "6249dee8d97a7391d356812e", "auther": "aaa", "brand": "QQQ", "category": [Object], "categoryName": "BackPack", "countInStock": 1, "dateCreated": "2022-04-03T17:51:01.813Z", "description": "description 4", "image": "https://i.pinimg.com/564x/df/3e/9f/df3e9f543c0f3ac7f8ed23193558b3d8.jpg", "images": [Array], "isFeatured": true, "name": "BackPack 1", "numReviews": 3, "price": 4, "rating": 2, "richDescription": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged." }, "quantity": 1 }, { "product": { "__v": 0, "_id": "624b31375bcffe79c45b4c96", "auther": "aaa", "brand": "QQQ", "category": [Object], "categoryName": "BackPack", "countInStock": 1, "dateCreated": "2022-04-04T17:31:40.870Z", "description": "description 4", "image": "https://i.pinimg.com/564x/df/3e/9f/df3e9f543c0f3ac7f8ed23193558b3d8.jpg", "images": [Array], "isFeatured": true, "name": "BackPack 1", "numReviews": 3, "price": 4, "rating": 2, "richDescription": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged." }, "quantity": 1 }]


  

  const storeOrder = async() =>{
    const data =
    {

      "status": "Pending",
      "orderItems": orderItems_data,
      "shippingAddress1": shippingAddress1,
      "shippingAddress2": shippingAddress2,
      "city": city,
      "zip": zip,
      "country": "Bangladesh",
      "phone": phone,
      "totalPrice": 350,
      "user": context.stateUser.user.userId,
      "__v": 0
    }
    // console.log(data);

  await  axios
    .post(`${baseURL}/orders`,data)
    .then((res)=>{
      // console.log(res);
      navigation.navigate('My Order')
    })
    .catch((error)=>{
      console.log(error);
    })
    
  }
  
  return (
    <ScrollView contentContainerStyle={styles.container}>
      <View style={styles.titleContainer}>
        <Text
          style={{
            fontSize: 25,
            marginVertical: 5,
            fontWeight: "bold",
            textAlign: "center",
          }}
        >
          Confirm Order
        </Text>
        {props.route.params ? (
          <Box
            key={uuid.v4()}
            style={{ borderWidth: 1, borderColor: "black", padding: 5 }}
          >
            <Text style={styles.title}>Shipping To : </Text>
            <Box style={{ padding: 8 }}>
              <Text>
                Address : {finalOrder?.order?.order?.shippingAddress1}{" "}
              </Text>
              <Text>
                Address2 :{finalOrder?.order?.order?.shippingAddress2}
              </Text>
              <Text>City : {finalOrder?.order?.order?.city}</Text>
              <Text>Zip Code : {finalOrder?.order?.order?.zip}</Text>
              <Text>Country : {finalOrder?.order?.order?.country}</Text>
            </Box>

            <Text style={styles.title}>Items:</Text>

            {ConfirmOrder?.order?.order?.orderItems.map(x => {
              // console.log('x', x.product);
              return (
                <Box style={styles.listItem} key={uuid.v4()} avatar>
                  <Box style={{ marginHorizontal: 10, marginVertical: 5 }}>
                    <Image
                      width="10"
                      height="10"
                      source={{ uri: x.product.image }}
                      alt={x.product.name}
                    />
                  </Box>
                  <Box style={styles.body}>
                    <Box>
                      <Text>{x.product.name}</Text>
                    </Box>
                    <Box>
                      <Text>$ {x.product.price}</Text>
                    </Box>
                  </Box>
                </Box>
              );
            })}
          </Box>
        ) : null}
      </View>
      <View style={{ alignItems: "center", margin: 20 }}>
        <Button onPress={storeOrder}>Place Order</Button>
      </View>
    </ScrollView>
  );
};

export default Confirm;

const styles = StyleSheet.create({
  listItem:{
    display:'flex',
    flexDirection:'row',
  },
  title:{
    fontSize:20,
    fontWeight:'bold',
  }
});
