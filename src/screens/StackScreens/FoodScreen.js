import { Box } from 'native-base';
import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import Food from '../../components/Food/Food';

const FoodScreen = () => {
  return (
    <Box flex="1" background="info.50">
      <Food/>
    </Box>
  );
};

export default FoodScreen;

const styles = StyleSheet.create({});
