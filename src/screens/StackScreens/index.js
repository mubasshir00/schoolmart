import BackpackScreen from './BackpackScreen';
import CompetitionScreen from './CompetitionScreen';
import CourseTrainingScreen from './CourseTrainingScreen';
import FeesPayScreen from './FeesPayScreen';
import FoodScreen from './FoodScreen';
import GamesScreen from './GamesScreen';
import GiftsScreen from './GiftsScreen';
import HealthCareScreen from './HealthCareScreen';
import HospitalityScreen from './HospitalityScreen';
import InsuranceScreen from './InsuranceScreen';
import JobsScreen from './JobsScreen';
import LibraryScreen from './LibraryScreen';
import LoanScreen from './LoanScreen';
import MobileRechargeScreen from './MobileRechargeScreen';
import MoreScreen from './MoreScreen';
import SavingsScreen from './SavingsScreen';
import ShoppingScreen from './ShoppingScreen';
import StationaryScreen from './StationaryScreen';
import TechnologyScreen from './TechnologyScreen';
import TravelScreen from './TravelScreen';
import WelfareScreen from './WelfareScreen';

export {
  BackpackScreen,
  CompetitionScreen,
  CourseTrainingScreen,
  FeesPayScreen,
  FoodScreen,
  GamesScreen,
  GiftsScreen,
  HealthCareScreen,
  HospitalityScreen,
  InsuranceScreen,
  JobsScreen,
  LibraryScreen,
  LoanScreen,
  MobileRechargeScreen,
  MoreScreen,
  SavingsScreen,
  ShoppingScreen,
  StationaryScreen,
  TechnologyScreen,
  TravelScreen,
  WelfareScreen,
};
