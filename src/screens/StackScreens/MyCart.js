import { useNavigation } from "@react-navigation/native";
import { Box, Image, ScrollView, Text } from "native-base";
import React, { useEffect, useState } from "react";
import { Dimensions, StyleSheet, View, TouchableOpacity } from "react-native";

const data = require("../../assets/LibraryAssets/Book.json");

const { width, height } = Dimensions.get("window");
import AntDesign from "react-native-vector-icons/AntDesign";
import Entypo from "react-native-vector-icons/Entypo";

const MyCart = () => {
  const [books, setBooks] = useState([]);

  const navigation = useNavigation();

  useEffect(() => {
    setBooks(data);
    return () => {
      setBooks([]);
    };
  }, []);

  return (
    <Box>
      <ScrollView>
        <Box paddingTop="3" background="info.50">
          {books.map(item => {
            const { id, name, image, price } = item;
            return (
              <Box key={id} px="3" paddingBottom="5">
                <Box flexDirection="row" justifyContent="space-between">
                  <Image
                    source={{ uri: image }}
                    rezieMode="cover"
                    width={width / 3.9}
                    height={width / 2.9}
                    alt={name}
                    borderRadius="10"
                  />
                  <Box
                    flexDirection="column"
                    justifyContent="space-between"
                    width={width / 1.6}
                  >
                    <Box>
                      <Text fontWeight="bold" fontSize="15">
                        {name}
                      </Text>
                      <Text color="grey" paddingTop="1">
                        {price}
                      </Text>
                    </Box>
                    <Box flexDirection="row" justifyContent="space-between">
                      <Box flexDirection="row" alignItems="center">
                        <Box
                          borderColor="black"
                          borderWidth="2"
                          borderRadius="5"
                        >
                          <AntDesign size={20} name="plus" />
                        </Box>
                        <Text mx="2" fontWeight="bold" fontSize="18">
                          100
                        </Text>
                        <Box
                          borderColor="black"
                          borderWidth="2"
                          borderRadius="5"
                        >
                          <AntDesign size={20} name="minus" />
                        </Box>
                      </Box>
                      <Box>
                        <Entypo size={30} name="cross" />
                      </Box>
                    </Box>
                  </Box>
                </Box>
              </Box>
            );
          })}
        </Box>
      </ScrollView>
      <TouchableOpacity
        onPress={() => navigation.navigate("Checkout")}
        style={styles.buyNowContainer}
      >
        <Box
          backgroundColor="#f01c2c"
          flexDirection="row"
          py="2"
          px="3"
          borderRadius="10"
          style={styles.buyNowWrapper}
        >
          <Box style={styles.buyNowbtn}>
            <Text color="white" fontSize="17" fontWeight="bold">
              Check Out
            </Text>
          </Box>
        </Box>
      </TouchableOpacity>
    </Box>
  );
};

export default MyCart;

const styles = StyleSheet.create({
  buyNowContainer: {
    position: "absolute",
    bottom: 0,
    left: 0,
    justifyContent: "center",
    alignItems: "center",
    width: width,
    backgroundColor: "white",
    flexDirection: "row",
  },
  buyNowWrapper: {
    width: width / 1.05,
    justifyContent: "space-around",
  },
  buyNowbtn: {
    // backgroundColor: '#eba7aa',
    borderRadius: 10,
    paddingHorizontal: 10,
    paddingVertical: 2,
  },
});
