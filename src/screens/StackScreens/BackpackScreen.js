import { useNavigation } from "@react-navigation/native";
import { Box, Icon, Input, ScrollView, Text } from "native-base";
import React from "react";
import { Dimensions, StyleSheet, TouchableOpacity, View } from "react-native";
import BackPackProducts from "../../components/BackPack/BackPackProducts";
import LibrarySlider from "../../components/LibraryComponents/LibrarySlider";
// import LibrarySlider from '../../components/LibraryComponents/LibrarySlider';
import Ionicons from "react-native-vector-icons/Ionicons";

const { width, height } = Dimensions.get("screen");

const BackpackScreen = () => {
  const navigation = useNavigation();

  return (
    <ScrollView background="info.50">
      <Box
        flexDirection="row"
        justifyContent="center"
        alignItems="center"
        marginBottom="0"
      ></Box>
      <LibrarySlider />
      <Box style={[styles.container]}>
        <TouchableOpacity
          onPress={() => navigation.navigate("Backpack Category")}
          style={[styles.item]}
        >
          <Text fontSize="15" color="white">
            School Bag
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => navigation.navigate("Backpack Category")}
          style={[styles.item]}
        >
          <Text fontSize="15" color="white">
            Travel Bag
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => navigation.navigate("Backpack Category")}
          style={[styles.item]}
        >
          <Text fontSize="15" color="white">
            Laptop Bag
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => navigation.navigate("Backpack Category")}
          style={[styles.item]}
        >
          <Text fontSize="15" color="white">
            Wallet & Side Bag
          </Text>
        </TouchableOpacity>
      </Box>
      <BackPackProducts />
    </ScrollView>
  );
};

export default BackpackScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "row",
    flexWrap: "wrap",
    alignItems: "flex-start",
    justifyContent: "center",
  },
  item: {
    width: width / 2.2,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#f01c2c",
    margin: 1,
    paddingVertical: 3,
    borderRadius: 4,
  },
});
