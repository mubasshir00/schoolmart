import React from 'react';
import {StyleSheet, View} from 'react-native';
import {AppProvider} from '../../components/Quiz/context';
import Quiz from '../../components/QuizComponent/LiveQuiz';
import QuizOption from '../../components/QuizComponent/QuizOption';

const quizCom = () =>{
  return (
    <AppProvider>
      <Quiz />
    </AppProvider>
  );
};

const CompetitionScreen = () => {
  return (
    <AppProvider>
      {/* <Quiz /> */}
      <QuizOption/>
    </AppProvider>
  );
};

export default CompetitionScreen;

const styles = StyleSheet.create({});
