import { Box } from 'native-base'
import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import CourseTraining from '../../components/Course&Training/CourseTraining'

const CourseTrainingScreen = () => {
    return (
        <Box flex='1' background='white'>
            <CourseTraining/>
        </Box>
    )
}

export default CourseTrainingScreen

const styles = StyleSheet.create({})
