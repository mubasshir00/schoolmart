import React, { useEffect, useState } from 'react';
import {StyleSheet, View} from 'react-native';
import {Box, ScrollView, Text} from 'native-base';
import LibrarySlider from '../../components/LibraryComponents/LibrarySlider';
import BookContainer from '../../components/LibraryComponents/Books/BookContainer';
import LibraryCom from '../../components/LibraryComponents/LibraryCom';
import baseURL from '../../assets/common/baseUrl';
import axios from 'axios';

const LibraryScreen = () => {
  const [data,setData] = useState([])
  useEffect( ()=>{
     axios
      .get(`${baseURL}/products?categoryName=Library`)
      .then((res)=>{
        // console.log(res?.data);
        setData(res?.data);
      })
      .catch((error)=>{
        console.log('Api Call Error');
      })
    return () =>{
      setData([])
    }
  },[])
  const libraryProduct = data.filter(function(el){
    return el.category.name === "Library"
  })

  // console.log(data);
  // console.log('libraryProduct', libraryProduct);
  return (
    <ScrollView background="info.50">
      <LibraryCom libraryProduct={data}/>
    </ScrollView>
  );
};

export default LibraryScreen;

const styles = StyleSheet.create({});
