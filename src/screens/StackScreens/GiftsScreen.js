import { Box, ScrollView } from 'native-base'
import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import Gits from '../../components/Gifts/Gits'

const GiftsScreen = () => {
    return (
        <ScrollView background="info.50">
        <Gits/>
        </ScrollView>
    )
}

export default GiftsScreen

const styles = StyleSheet.create({})
