import { Box, ScrollView } from 'native-base'
import React, { useContext } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import LoginScreen from '../../Authentication/LoginScreen'
import UserProfile from '../../Authentication/UserProfile'
import AuthGlobal from '../../context/store/AuthGlobal'

const ProfileScreen = () => {
    const context = useContext(AuthGlobal);
    return (
        <ScrollView background='white'>
            {
                context.stateUser.user.userId ? <UserProfile/> : <LoginScreen />  
            }
        </ScrollView>
    )
}

export default ProfileScreen

const styles = StyleSheet.create({})
