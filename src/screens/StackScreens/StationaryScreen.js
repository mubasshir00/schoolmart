import React from 'react'
import { StyleSheet, View } from 'react-native'
import Stationary from '../../components/Stationary/Stationary'
import { Box, ScrollView, Text } from 'native-base';

const StationaryScreen = () => {
    return (
        <ScrollView background="info.50">
            <Stationary/>
        </ScrollView>
    )
}

export default StationaryScreen

const styles = StyleSheet.create({})
