import { Dimensions,StyleSheet, View } from 'react-native';
import React from 'react';
import { Box, Image, ScrollView, Text } from 'native-base';
const { width, height } = Dimensions.get('screen');

const notifications=[
  {
    "id" : 1,
    "title_image": "https://i.pinimg.com/564x/18/d3/b2/18d3b21ee334e3621b59f5d10b54c41e.jpg",
    "header_image": "https://i.pinimg.com/236x/2b/b6/5a/2bb65a369bc5024cec0130d731db244a.jpg",
    "short_description": "আপনার আজকের অফার",
    "description": "রমজান মাস চলাকালীন আমরা একটি ক্যাম্পেইন চালু করছি যেখানে অথোরাইজ্‌ড স্মার্টফোন কিনে  সফলভাবে ট্যাগ করলে গ্রাহকগণ ঈদের পর একটি স্পেশাল গিফ্‌ট প্যাক পাবেন।"
  }, {
    "id" :2,
    "title_image": "https://i.pinimg.com/564x/18/d3/b2/18d3b21ee334e3621b59f5d10b54c41e.jpg",
    "header_image": "https://i.pinimg.com/236x/2b/b6/5a/2bb65a369bc5024cec0130d731db244a.jpg",
    "short_description": "আপনার আজকের অফার",
    "description": "রমজান মাস চলাকালীন আমরা একটি ক্যাম্পেইন চালু করছি যেখানে অথোরাইজ্‌ড স্মার্টফোন কিনে  সফলভাবে ট্যাগ করলে গ্রাহকগণ ঈদের পর একটি স্পেশাল গিফ্‌ট প্যাক পাবেন।"
  }
]

const NotificationScreen = () => {
  return (
    <ScrollView background="info.50">
      <Box>
        {
          notifications.map((i)=>{
            const { id, title_image, short_description, header_image, description} = i;
            return (
              <Box style={styles.container} key={id}>
                <Image
                  source={{ uri: title_image }}
                  rezieMode="cover"
                  alt="a"
                  width={width}
                  height={width / 5.4}
                />
                <Box style={styles.lower_container}>
                  <Image
                    source={{ uri: header_image }}
                    rezieMode="cover"
                    alt="a"
                    width={width /6.1}
                    height={width / 5.4}
                   
                  />
                  <Box style={{paddingLeft:5,width:"86%"}}>
                    <Box>
                      <Text style={{fontSize:17,fontWeight:'bold'}}>{short_description}</Text>
                    </Box>
                    <Box>
                      <Text style={{fontSize:12}}>{description}</Text>
                    </Box>
                  </Box>
                </Box>
              </Box>
            )
          })
        }
      </Box>
    </ScrollView>
  );
};

export default NotificationScreen;

const styles = StyleSheet.create({
  container:{
    display:'flex',
    flexDirection:'column',
    paddingVertical:5,
    borderBottomColor:'grey',
    borderBottomWidth:1
  },
  lower_container:{
    display:'flex',
    flexDirection:'row',
    alignItems:'center',
    // paddingHorizontal:'10'
    paddingVertical:5,
    paddingHorizontal:5
    }
});
