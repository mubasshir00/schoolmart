import { useNavigation } from '@react-navigation/native';
import React from 'react';
import {
  Image,
  FlatList,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import data from '../../../data';
import { Typography } from '../../theme/Typography';

const categroyData = data.categoryData.filter(e => e.screen !== 'More');

const MoreScreen = () => {
  const navigation = useNavigation();

  const onPress = screenName => {
    navigation.navigate(screenName);
  };

  return (
    <View style={styles.container}>
      <FlatList
        data={categroyData}
        keyExtractor={item => item.id}
        renderItem={({ item }) => (
          <TouchableOpacity
            onPress={() => {
              onPress(item.screen);
            }}
            style={styles.cardContainer}>
            <View>
              <Image
                style={styles.imageContainer}
                source={item.image}
                resizeMode={'cover'}
              />
            </View>
            <Text>{item.title}</Text>
          </TouchableOpacity>
        )}
        numColumns={3}
      />
    </View>
  );
};

export default MoreScreen;

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    alignItems: 'center',
    borderBottomColor: 'white',
    borderBottomWidth: 3,
    // paddingBottom:20,
    paddingTop: 20,
    // paddingLeft:20,
    // paddingRight:20,
    paddingHorizontal: 30,
    width: '100%',
    flex: 1
  },
  cardContainer: {
    alignItems: 'center',
    // paddingBottom: 20,
    width: 100,
    padding: Typography.iconWithText_Padding
  },
  imageContainer: {
    width: Typography.iconImageWidth,
    height: Typography.iconImageHeight,
  },
});
