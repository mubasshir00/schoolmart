import React, { useContext, useState } from 'react';
import { FlatList, StyleSheet, View, Dimensions } from 'react-native';
import HomeSmallCard from '../components/Card/HomeSmallCard';
import Carousel from '../components/Carousel/Carousel';
import Category from '../components/Category/Category';
import 'react-native-gesture-handler';
import NavBar from '../components/NavBar/NavBar';
import { ScrollView, Text } from 'native-base';
import Offers from '../components/Offers/Offers';
import AuthGlobal from '../context/store/AuthGlobal';
const { width, height } = Dimensions.get('window')
console.log('height',height);
const MyMart = [
  {
    id: 1,
    title: 'My Mart',
    image: require('../assets/CardAssets/MyMart/shopping.png'),
  },
  {
    id: 2,
    title: 'Saved Shop',
    image: require('../assets/CardAssets/MyMart/online-shop.png'),
  },
  {
    id: 3,
    title: 'Regular Shop',
    image: require('../assets/CardAssets/MyMart/schedule.png'),
  },
  {
    id: 4,
    title: 'Priyo Shop',
    image: require('../assets/CardAssets/MyMart/favorite.png'),
  },
];


const offerObject = [
  {
    "id": "1",
    "image": "https://i.pinimg.com/236x/76/7c/0b/767c0b605e75e6d49a53141e2de695f1.jpg",
  },
  {
    "id": "2",
    "image": "https://i.pinimg.com/236x/76/7c/0b/767c0b605e75e6d49a53141e2de695f1.jpg"
  },
  {
    "id": "3",
    "image": "https://i.pinimg.com/236x/76/7c/0b/767c0b605e75e6d49a53141e2de695f1.jpg",
  },
  {
    "id": "4",
    "image": "https://i.pinimg.com/236x/76/7c/0b/767c0b605e75e6d49a53141e2de695f1.jpg"
  }
]


const HomeScrollComponent = () => {
  // let content =  <>
  //         <Category/>
  //         <HomeSmallCard Header={'My Mart'} cardData={MyMart}/>
  //         <Carousel/>
  //         <HomeSmallCard Header={'Offers'} cardData={myOffer}/>
  //          </>

  const context = useContext(AuthGlobal)

  console.log(context.stateUser.user.userId);

  return (
    <ScrollView>
      <NavBar />
      <View style={styles.category}>
        <Category />
      </View>
      <View style={styles.homeScroll}>
        <HomeSmallCard Header={'Suggestions'} cardData={MyMart} />
      </View>
      <Carousel />
      <Offers style={{height:'40%'}} Header={'Offers'} cardData={offerObject}/>
      {/* <HomeSmallCard Header={'Offers'} cardData={myOffer} /> */}
    </ScrollView>
  );
};

const HomeScreen = () => {
  return <FlatList ListEmptyComponent={HomeScrollComponent} />;
};

export default HomeScreen;

const styles = StyleSheet.create({
  category:{
    height:'35%',
    // marginBottom:10
  },
  homeScroll:{
    // height:height*(2/10),
  }
});
