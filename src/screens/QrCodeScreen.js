import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import QrCodeScanner from '../components/Qrcodescanner/QrCodeScanner'

const QrCodeScreen = () => {
    return (
        <View>
            <QrCodeScanner/>
        </View>
    )
}

export default QrCodeScreen

const styles = StyleSheet.create({})
