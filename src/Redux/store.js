import { createStore,combineReducers,applyMiddleware, compose } from "redux";
import  thunkMiddleware  from "redux-thunk";

import cartItems from "./Reducers/cartItem";

const reducers = combineReducers({
    //cart Reducer
    cartItems:cartItems
})

const store = createStore(
    reducers,
    
)

export default store